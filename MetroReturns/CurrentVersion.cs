﻿namespace MetroReturns;

internal class CurrentVersion
{
	public const uint VERSION = 167;

	// ReSharper disable once InconsistentNaming
	public static  string  VERSION_NUMBER => _VersionNumber ??= $"Vsn: {(decimal)VERSION / 100:N2}";

	private static string? _VersionNumber;

	public const string PACKAGE_NAME = "com.idsapp.metroreturns.apk";
}