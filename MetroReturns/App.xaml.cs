﻿using Resources.Languages;
using Application = Xamarin.Forms.Application;

namespace MetroReturns;

public partial class App : Application
{
	public static LANGUAGE Language
	{
		get;
		set
		{
			if( value != field )
			{
				field = value;
				Instance?.SetLanguage( value );
			}
		}
	} = LANGUAGE.ENGLISH;

	private static App? Instance;

	private static LANGUAGE CurrentLanguage = LANGUAGE.ENGLISH; // From Xaml

	private void SetLanguage( LANGUAGE languageCode )
	{
		try
		{
			if( languageCode != CurrentLanguage )
			{
				var Dict = CurrentLanguage switch
				           {
					           LANGUAGE.FRENCH => typeof( French ),
					           _               => typeof( English )
				           };

				// Find and remove the existing dictionary with the same source
				var ExistingDict = ( from M in Resources.MergedDictionaries
				                     where M.GetType() == Dict
				                     select M ).FirstOrDefault();

				if( ExistingDict is not null )
					Resources.MergedDictionaries.Remove( ExistingDict );

				ResourceDictionary GetResourceDictionary( LANGUAGE language )
				{
					return language switch
					       {
						       LANGUAGE.FRENCH => new French(),
						       _               => new English()
					       };
				}

				Resources.MergedDictionaries.Add( GetResourceDictionary( languageCode ) );
				CurrentLanguage = languageCode;
			}
		}
		catch( Exception Exception )
		{
			Console.WriteLine( Exception );
		}
	}

	public App()
	{
		Instance = this;
		InitializeComponent();
		MainPage = new MainPage();
	}

	public enum LANGUAGE : byte
	{
		UNDEFINED,
		ENGLISH,
		FRENCH
	}

	protected override void OnStart()
	{
	}

	protected override void OnSleep()
	{
	}

	protected override void OnResume()
	{
	}
}