﻿using File = System.IO.File;

namespace MetroReturns.ServerUpdates;

public abstract class IdBase
{
	public Guid Id { get; } = Guid.NewGuid();
}

public class AssetTrip : IdBase
{
	public string AccountId,
	              TripId;

	public IEnumerable<string> AssetBarcodes;

	public AssetTrip( string accountId, string tripId, IEnumerable<string> assetBarcodes )
	{
		AccountId     = accountId;
		TripId        = tripId;
		AssetBarcodes = assetBarcodes;
	}
}

public class RemoteTrip : IdBase
{
	public remoteTrip Trip;

	public RemoteTrip( remoteTrip t )
	{
		Trip = t;
	}
}

public class RemoteTripDetailed : IdBase
{
	public remoteTripDetailed Trip;

	public RemoteTripDetailed( remoteTripDetailed t )
	{
		Trip = t;
	}
}

public class PickupTrips : IdBase
{
	public List<string>   TripIds;
	public DateTimeOffset PickupDateTime;

	public PickupTrips( List<string> tripIds, DateTimeOffset pickupDateTime )
	{
		TripIds        = tripIds;
		PickupDateTime = pickupDateTime;
	}
}

public abstract class UpdateViewModel : ViewModelBase
{
	private new const  string PRODUCT_NAME = "MetroReturns";
	protected override string ProductName => PRODUCT_NAME;

	protected override void OnInitialised()
	{
		base.OnInitialised();

		LoadUpdateList();

		Events.OnPingEvent.Add( ( _, online ) =>
		                        {
			                        if( online )
				                        UpdateServer();
		                        } );
	}

	public class SaveEntry
	{
		public Type?  Type        { get; set; }
		public string EntryAsJson { get; set; } = "";
	}

	public class SaveList : List<SaveEntry>;

	private static readonly List<IdBase> UpdatesList = [];

	private static string BaseFolder = "";

	private static readonly object LockObject = new();

	private static volatile bool InUpdate;

	private static string SaveFileName
	{
		get
		{
			if( field.IsNullOrEmpty() )
			{
				var AppData = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
				BaseFolder = $"{AppData}\\{PRODUCT_NAME}";

				if( !Directory.Exists( BaseFolder ) )
					Directory.CreateDirectory( BaseFolder );

				field = $"{BaseFolder}\\UpdateList.{SETTINGS_FILE_EXTENSION}";
			}

			return field;
		}
	} = "";

	public void UpdateFromObject( IdBase updateObject )
	{
		lock( LockObject )
		{
			UpdatesList.Add( updateObject );
			SaveUpdateList();
		}

		UpdateServer();
	}

	private static void SaveUpdateList()
	{
		lock( LockObject )
		{
			try
			{
				var SaveList = new SaveList();

				foreach( var Entry in UpdatesList )
				{
					SaveList.Add( new SaveEntry
					              {
						              Type        = Entry.GetType(),
						              EntryAsJson = JsonConvert.SerializeObject( Entry )
					              } );
				}

				var SerializedObject = JsonConvert.SerializeObject( SaveList );
				File.WriteAllText( SaveFileName, SerializedObject );
			}
			catch( Exception Exception )
			{
				Console.WriteLine( Exception );
			}
		}
	}


	private static void LoadUpdateList()
	{
		lock( LockObject )
		{
			var FileName = SaveFileName;
			var Temp     = new List<IdBase>();

			if( File.Exists( FileName ) )
			{
				var SerializedObject = File.ReadAllText( FileName );
				var Lst              = JsonConvert.DeserializeObject<SaveList>( SerializedObject );

				if( Lst is not null )
				{
					foreach( var Entry in Lst )
					{
						if( Entry.Type is not null )
						{
							var Obj = JsonConvert.DeserializeObject( Entry.EntryAsJson, Entry.Type );

							if( Obj is IdBase IdBase )
							{
								var Found = false;

								foreach( var Base in UpdatesList )
								{
									if( Base.Id == IdBase.Id )
									{
										Found = true;
										break;
									}
								}

								if( !Found )
									Temp.Add( IdBase );
							}
						}
					}
				}

				foreach( var IdBase in UpdatesList )
					Temp.Add( IdBase );

				UpdatesList.Clear();
				UpdatesList.AddRange( Temp );
			}
		}
	}

	// ReSharper disable once NotAccessedField.Local
	private static void UpdateServer()
	{
		if( InUpdate )
			return;

		InUpdate = true;

		lock( LockObject )
		{
			if( UpdatesList.Count <= 0 )
			{
				InUpdate = false;
				return;
			}
		}

		Task.Run( () =>
		          {
			          Monitor.Enter( LockObject );

			          try
			          {
				          while( UpdatesList.Count > 0 )
				          {
					          try
					          {
						          var UpdObj = UpdatesList[ 0 ];

						          Monitor.Exit( LockObject );

						          switch( UpdObj )
						          {
						          case RemoteTrip { Trip: not null } Tr:
							          if( !Service.Client.UpdateTrip( Tr.Trip ) )
								          return;
							          break;

						          case PickupTrips Pt:
							          Service.Client.PickupTrips( Pt.TripIds, Pt.PickupDateTime );
							          break;

						          case RemoteTripDetailed { Trip: not null } Tr:
							          if( !Service.Client.UpdateTripDetailedQuick( Tr.Trip ) )
								          return;
							          break;

						          case AssetTrip At:
							          Service.Client.UpdateRbhAssets( At.AccountId, At.TripId, At.AssetBarcodes );
							          break;
						          }
					          }
					          catch
					          {
						          return;
					          }

					          Monitor.Enter( LockObject );

					          UpdatesList.RemoveAt( 0 );
					          SaveUpdateList();
				          }

				          Monitor.Exit( LockObject );
			          }
			          finally
			          {
				          InUpdate = false;
			          }
		          } );
	}
}