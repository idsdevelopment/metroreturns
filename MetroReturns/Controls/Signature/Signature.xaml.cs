﻿namespace MetroReturns.Components;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Signature : ContentView
{
	public OnCancelEvent? OnCancel;
	public OnOkEvent?     OnOk;

	public Signature()
	{
		InitializeComponent();

		if( Root.BindingContext is SignatureViewModel Model )
		{
			Model.OnGetPoints = () => SignaturePad.Points;

			Model.OnOk = signature =>
			             {
				             OnOk?.Invoke( signature );
				             SignaturePad.Clear();
			             };

			Model.OnCancel = () =>
			                 {
				                 OnCancel?.Invoke();
				                 SignaturePad.Clear();
			                 };
		}
	}

	public void Clear()
	{
		SignaturePad.Clear();
	}

	public delegate void OnCancelEvent();

	public delegate void OnOkEvent( SignatureResult signature );
}