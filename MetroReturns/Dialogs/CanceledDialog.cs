﻿namespace MetroReturns.Dialogs;

public static class CanceledDialog
{
	public static Task Execute( string messageKey, bool errorSound = true )
	{
		if( errorSound )
			Sounds.PlayBadScanSound();

		return MainPage.Instance.DisplayAlert( Globals.Resources.GetStringResource( "Canceled" ),
											   Globals.Resources.GetStringResource( messageKey ),
											   Globals.Resources.GetStringResource( "Ok" ) );
	}
}