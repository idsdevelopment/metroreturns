﻿namespace MetroReturns.Dialogs;

public static class CancelDialog
{
	public static Task<bool> Execute() => ConfirmDialog.Execute( "CancelMessage" );
}