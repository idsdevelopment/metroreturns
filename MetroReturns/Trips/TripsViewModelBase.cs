﻿namespace MetroReturns.Trips;

public abstract class TripsViewModelBase : ViewModelBase
{
	public bool Enabled
	{
		get { return Get( () => Enabled, false ); }
		set { Set( () => Enabled, value ); }
	}

#region Filters
	public Service.STATUS[] StatusFilter = CurrentTripsForDriver.DefaultStatusFilter;
#endregion

	protected volatile bool DisableUpdate;

	protected static bool RemoveTrip( string tripId ) => CurrentTripsForDriver.RemoveTrip( tripId );

	protected static void ReplaceTrips( ICollection<TripDisplayEntry> trips ) => CurrentTripsForDriver.ReplaceTrips( trips );

	protected virtual void OnNewTrips( IList<TripDisplayEntry> newTrips )
	{
	}

	protected virtual void OnUpdateTrips( IList<TripDisplayEntry> updatedTrips )
	{
	}

	protected virtual void OnDeleteTrips( IList<string> deletedTrips )
	{
	}

	protected override void OnInitialised()
	{
		base.OnInitialised();

		Trips = CurrentTripsForDriver.Trips;

		CurrentTripsForDriver.NewTripsEvent.Add( ( _, list ) =>
		                                         {
			                                         if( !DisableUpdate )
			                                         {
				                                         lock( TripLockObject )
				                                         {
					                                         var Trps = Trips;

					                                         foreach( var Entry in FilterTrips( list ) )
						                                         Trps.Add( Entry );
				                                         }

				                                         OnNewTrips( list );
			                                         }
		                                         } );

		CurrentTripsForDriver.UpdatedTripsEvent.Add( ( _, list ) =>
		                                             {
			                                             if( !DisableUpdate )
			                                             {
				                                             lock( TripLockObject )
				                                             {
					                                             var List = FilterTrips( list );
					                                             var Trps = Trips;

					                                             for( int Ndx = 0, C = Trps.Count; Ndx < C; Ndx++ )
					                                             {
						                                             var Trip = Trps[ Ndx ];

						                                             // ReSharper disable once PossibleMultipleEnumeration
						                                             foreach( var Entry in List )
						                                             {
							                                             if( Trip.TripId == Entry.TripId )
							                                             {
								                                             Trps[ Ndx ] = Entry;
								                                             break;
							                                             }
						                                             }
					                                             }
				                                             }

				                                             OnUpdateTrips( list );
			                                             }
		                                             } );

		CurrentTripsForDriver.DeletedTripsEvent.Add( ( _, list ) =>
		                                             {
			                                             if( !DisableUpdate )
			                                             {
				                                             lock( TripLockObject )
				                                             {
					                                             var Trps = Trips;

					                                             for( var Ndx = Trps.Count; --Ndx >= 0; )
					                                             {
						                                             var TripId = Trps[ Ndx ].TripId;

						                                             if( list.Any( id => TripId == id ) )
							                                             Trps.RemoveAt( Ndx );
					                                             }

					                                             OnDeleteTrips( list );
				                                             }
			                                             }
		                                             } );
	}

	public void WaitForTrips( Action<bool> onWaitChanged )
	{
		Tasks.RunVoid( () =>
		               {
			               var Waiting = false;

			               var SaveDisable = DisableUpdate;
			               DisableUpdate = false;

			               try
			               {
				               CurrentTripsForDriver.PollTrips();

				               while( Enabled )
				               {
					               var TripCount = FilterTrips().Count;

					               switch( Waiting )
					               {
					               case false when TripCount == 0:
						               Waiting = true;
						               onWaitChanged( true );
						               break;

					               case false when TripCount > 0:
					               case true when TripCount > 0:
						               onWaitChanged( false );
						               return;
					               }
					               Thread.Sleep( 15_000 );
				               }
			               }
			               finally
			               {
				               DisableUpdate = SaveDisable;
			               }
		               } );
	}

	public bool HasTrips( Service.STATUS[]? filter = null )
	{
		filter ??= StatusFilter;
		return Trips.Any( trip => filter.Contains( trip.Status ) );
	}

#region Trip Filter
	public Func<TripDisplayEntry, bool>? OnFilterTripCallback;

	protected virtual bool OnFilterTrip( TripDisplayEntry trip ) => OnFilterTripCallback?.Invoke( trip ) ?? true;

	private IEnumerable<TripDisplayEntry> FilterTrips( IEnumerable<TripDisplayEntry> trips, Service.STATUS[]? filter = null )
	{
		filter ??= StatusFilter;

		return from T in trips
		       where filter.Contains( T.Status ) && OnFilterTrip( T )
		       select T;
	}

	public virtual ObservableCollection<TripDisplayEntry> FilterTrips( Service.STATUS[]? filter = null )
	{
		lock( TripLockObject )
			return new ObservableCollection<TripDisplayEntry>( FilterTrips( CurrentTripsForDriver.Trips, filter ) );
	}

	public void RefreshTrips()
	{
		lock( TripLockObject )
			Trips = FilterTrips();
	}
#endregion

#region Trips
	protected readonly object TripLockObject = new();

	public ObservableCollection<TripDisplayEntry> Trips
	{
		get { return Get( () => Trips, () => FilterTrips() ); }
		set { Set( () => Trips, value ); }
	}
#endregion
}