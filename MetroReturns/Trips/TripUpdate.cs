﻿namespace MetroReturns.Trips;

public class TripUpdate : UpdateViewModel
{
	private static TripUpdate? _TrUpdate;
	private static TripUpdate  TrUpdate => _TrUpdate ??= new TripUpdate();

	internal static void Update( remoteTrip t )
	{
		TrUpdate.UpdateFromObject( new RemoteTrip( t ) );
	}

	internal static void Update( remoteTripDetailed t )
	{
		TrUpdate.UpdateFromObject( new RemoteTripDetailed( t ) );
	}

	internal static void Update( TripDisplayEntry t, SignatureResult signature, string driver, Service.STATUS status )
	{
		Update( t.ConvertToRemoteTrip( signature, driver, status ) );
	}

	internal static void PickupTrips( List<string> tripIds, DateTimeOffset pickupDateTime )
	{
		TrUpdate.UpdateFromObject( new PickupTrips( tripIds, pickupDateTime ) );
	}

	internal static void Update( IEnumerable<TripDisplayEntry> trips, SignatureResult signature, string driver, Service.STATUS status )
	{
		foreach( var Trip in trips )
			Update( Trip, signature, driver, status );
	}

	public static void UpdateRbhAssets( string accountId, string tripId, IEnumerable<string> assetBarcodes )
	{
		TrUpdate.UpdateFromObject( new AssetTrip( accountId, tripId, assetBarcodes ) );
	}
}