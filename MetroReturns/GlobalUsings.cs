// Global using directives

global using System;
global using System.Threading.Tasks;
global using System.Windows.Input;
global using XamlViewModel.Xamarin;
global using Xamarin.Forms;
global using Xamarin.Forms.Xaml;
global using System.Collections.Generic;
global using System.IO;
global using System.Threading;
global using Globals;
global using IdsRemoteService;
global using IdsRemoteService.org.internetdispatcher.jbtest2;
global using Newtonsoft.Json;
global using Utils;
global using System.Collections.ObjectModel;
global using System.Linq;
global using Globals.Trips;
global using MetroReturns.Trips;
global using System.ComponentModel;
global using System.Runtime.CompilerServices;
global using CommandViewModel.Annotations;
global using MetroReturns.Dialogs;
global using MetroReturns.ServerUpdates;