﻿namespace Resources.Styles
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class Components : ResourceDictionary
	{
		public Components()
		{
			InitializeComponent();
		}
	}
}