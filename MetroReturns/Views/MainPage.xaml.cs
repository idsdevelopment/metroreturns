﻿using AppUpdater;
using MetroReturns.Views;
using MetroReturns.Views.Claims;
using MetroReturns.Views.DriverVerification;
using MetroReturns.Views.Warehouse;
using Transitions;

namespace MetroReturns;

public partial class MainPage : ContentPage
{
	public static MainPage Instance { get; private set; } = null!;

	private static readonly Rectangle LayoutRectangle = new( 0, 0, 1, 1 );

	private readonly MainPageViewModel Model;

	private MainPageViewModel.STATE CurrentState = MainPageViewModel.STATE.SIGN_IN;

	public MainPage()
	{
		Instance = this;
		InitializeComponent();

		// Ping can happen before it's setup
		CurrentView = SignIn;

		var M = Model = (MainPageViewModel)Root.BindingContext;

		M.Show = async state =>
		         {
			         switch( state )
			         {
			         case MainPageViewModel.STATE.SIGN_IN:
				         CurrentView = SignIn;
				         break;

			         case MainPageViewModel.STATE.HOME when CurrentState is MainPageViewModel.STATE.SIGN_IN:
				         await LoadView( SignIn, new Home() );
				         break;

			         case MainPageViewModel.STATE.HOME:
				         await LoadView( new Home() );
				         break;

			         case MainPageViewModel.STATE.RETURNS:
				         await LoadView( new Returns() );
				         break;

			         case MainPageViewModel.STATE.DSD_DELIVERIES:
				         await LoadView( new DsdDeliveries() );
				         break;

			         case MainPageViewModel.STATE.DRIVER_VERIFICATION:
				         await LoadView( new Verification() );
				         break;

			         case MainPageViewModel.STATE.DRIVER_CLAIMS:
				         await LoadView( new Claims() );
				         break;

			         case MainPageViewModel.STATE.WAREHOUSE_CLAIMS:
				         await LoadView( new WarehouseClaims() );
				         break;

			         case MainPageViewModel.STATE.NO_INTERNET when CurrentState is MainPageViewModel.STATE.RETURNS or MainPageViewModel.STATE.SIGN_IN:
				         await Root.RaiseAndFade( CurrentView, LostInternet );
				         return;

			         case MainPageViewModel.STATE.NO_INTERNET:
				         Events.OnInternetEvent.Value = false;
				         return;

			         case MainPageViewModel.STATE.INTERNET when CurrentState is MainPageViewModel.STATE.RETURNS or MainPageViewModel.STATE.SIGN_IN:
				         await Root.RaiseAndFade( LostInternet, CurrentView );
				         return;

			         case MainPageViewModel.STATE.INTERNET:
				         Events.OnInternetEvent.Value = true;
				         return;
			         }
			         CurrentState = state;
		         };

		M.InitUpdates = () =>
		                {
			                Updates.Init( $"{Service.METRO}Returns", CurrentVersion.VERSION, CurrentVersion.PACKAGE_NAME,
			                              () =>
			                              {
				                              M.UpdateAvailable    = true;
				                              M.UpdateNotAvailable = false;
			                              } );
		                };
	}

	public static void Home()
	{
		Instance.Model.Show( MainPageViewModel.STATE.HOME );
	}

	public static void DsdDeliveries()
	{
		Instance.Model.Show( MainPageViewModel.STATE.DSD_DELIVERIES );
	}

#region Load View
	private View CurrentView;

	private async Task RaiseAndFade( View fromView, View toView )
	{
		CurrentView = toView;
		await Root.RaiseAndFade( fromView, toView );
	}

	private async Task LoadView( View fromView, View toView )
	{
		var Ch = LoadArea.Children;
		toView.IsVisible = false;
		Ch.Add( toView );
		AbsoluteLayout.SetLayoutFlags( toView, AbsoluteLayoutFlags.All );
		AbsoluteLayout.SetLayoutBounds( toView, LayoutRectangle );
		await RaiseAndFade( fromView, toView );
		Ch.Remove( fromView );
		GC.Collect( 2 );
	}

	private async Task LoadView( View toView )
	{
		await LoadView( CurrentView, toView );
	}
#endregion
}