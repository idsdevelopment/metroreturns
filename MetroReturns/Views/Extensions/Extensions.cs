﻿// ReSharper disable InconsistentNaming

namespace MetroReturns.Views;

public static class Extensions
{
	public const string CWD_SHIPMENT = "CWDShipment",
	                    RETURN       = "return";

	public static bool IsReturn( this string serviceLevel ) => serviceLevel.Compare( RETURN );
	public static bool IsReturn( this remoteTrip trip ) => trip.serviceLevel.IsReturn();
	public static bool IsReturn( this TripDisplayEntry trip ) => trip.ServiceLevel.IsReturn();


	public static bool IsCWD( this string serviceLevel ) => serviceLevel.Compare( CWD_SHIPMENT );
	public static bool IsCWD( this remoteTrip trip ) => trip.serviceLevel.IsCWD();
	public static bool IsCWD( this TripDisplayEntry trip ) => trip.ServiceLevel.IsCWD();

	private static bool Compare( this string s1, string s2 ) => s1.Trim().Compare( s2, StringComparison.OrdinalIgnoreCase ) == 0;
}