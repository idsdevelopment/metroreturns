﻿using static IdsRemoteService.Service;
using Keyboard = Globals.Keyboard;

// ReSharper disable UnusedMember.Global

namespace MetroReturns.Views;

internal class DsdDeliveriesViewModel : TripsViewModelBase
{
	private const string SCANNED_DROP_OFF = "Asset Drop-off";

#region Asset Drop Offs
	public List<string> AssetDropOffs
	{
		get { return Get( () => AssetDropOffs, [] ); }
		set { Set( () => AssetDropOffs, value ); }
	}
#endregion

	protected override void OnInitialised()
	{
		base.OnInitialised();
		StatusFilter = [STATUS.DISPATCHED, STATUS.PICKED_UP];
		State        = STATE.WAITING_FOR_TRIPS;
	}

	public bool TripFilterCallback( TripDisplayEntry entry ) => entry.IsAsset || entry.IsDsd || entry.IsCTT || entry.IsReturn;

#region Tapped Returns
	public bool OnReturnTapped( TripDisplayEntry entry )
	{
		if( entry.Reference.IsNullOrWhiteSpace() )
		{
			TappedReturnTrip.Clear();
			TappedReturnTrip.Add( entry );
			State = STATE.TAPPED_RETURN;
			return true;
		}
		return false;
	}
#endregion

#region Trips
	public ObservableCollection<TripDisplayEntry> DisplayTrips
	{
		get { return Get( () => DisplayTrips, [] ); }
		set { Set( () => DisplayTrips, value ); }
	}

	public bool TripsVisible
	{
		get { return Get( () => TripsVisible, true ); }
		set { Set( () => TripsVisible, value ); }
	}

	public bool TripsNotVisible
	{
		get { return Get( () => TripsNotVisible, false ); }
		set { Set( () => TripsNotVisible, value ); }
	}

	[DependsUpon( nameof( TripsVisible ) )]
	public void WhenTripsVisibleChanges()
	{
		TripsNotVisible = !( PodSignVisible || TripsVisible );
	}

	public bool Deliveries
	{
		get { return Get( () => Deliveries, true ); }
		set { Set( () => Deliveries, value ); }
	}

#region Pod & Signature
	private SignatureResult? Signature;

	public Action? OnShowSignature;

	private void EnableNext()
	{
		NextButtonEnabled = Signature is not null && ( POD != "" );
	}

	public void OnSignatureComplete( SignatureResult? sig )
	{
		Signature        = sig;
		SignatureVisible = false;
		PodSignVisible   = true;
		EnableNext();
	}

	public bool PodSignVisible
	{
		get { return Get( () => PodSignVisible, false ); }
		set { Set( () => PodSignVisible, value ); }
	}

	public bool SignatureVisible
	{
		get { return Get( () => SignatureVisible, false ); }
		set { Set( () => SignatureVisible, value ); }
	}

	[DependsUpon( nameof( SignatureVisible ) )]
	public void WhenSignatureVisibleChanges()
	{
		if( SignatureVisible )
			OnShowSignature?.Invoke();
	}

	[DependsUpon( nameof( PodSignVisible ) )]
	public void WhenPodSignChanges()
	{
		if( PodSignVisible )
		{
			TripsVisible = false;
			Deliveries   = false;
		}
	}

	public ICommand OnSignature => Commands[ nameof( OnSignature ) ];

	public void Execute_OnSignature()
	{
		PodSignVisible   = false;
		SignatureVisible = true;
	}

	// ReSharper disable once InconsistentNaming
	public string POD
	{
		get { return Get( () => POD, "" ); }
		set { Set( () => POD, value ); }
	}

	[DependsUpon350( nameof( POD ) )]
	// ReSharper disable once InconsistentNaming
	public void WhenPODChanges()
	{
		EnableNext();
	}
#endregion

	public bool ShowScanner
	{
		get { return Get( () => ShowScanner, true ); }
		set { Set( () => ShowScanner, value ); }
	}

	public ObservableCollection<string> ScannedDropOffs
	{
		get { return Get( () => ScannedDropOffs, [] ); }
		set { Set( () => ScannedDropOffs, value ); }
	}
#endregion

#region Buttons
	public Func<Task<bool>>? OnConfirmCancel;
	public Func<Task>?       OnCanceled;

	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public async void Execute_OnCancel()
	{
		try
		{
			if( OnConfirmCancel is not null )
			{
				if( State == STATE.TAPPED_RETURN )
					NextState();

				else if( await OnConfirmCancel() )
				{
					if( State == STATE.LOCATION )
						MainPage.Home();
					else
						State = STATE.LOCATION;
				}
			}
		}
		catch
		{
		}
	}

	public string NextButtonText
	{
		get { return Get( () => NextButtonText, "" ); }
		set { Set( () => NextButtonText, value ); }
	}

	public bool NextButtonVisible
	{
		get { return Get( () => NextButtonVisible, false ); }
		set { Set( () => NextButtonVisible, value ); }
	}

	public bool NextButtonEnabled
	{
		get { return Get( () => NextButtonEnabled, false ); }
		set { Set( () => NextButtonEnabled, value ); }
	}

#region Next Button
	public Func<string, Task<bool>>? OnConfirmAssets;

	public ICommand OnNext => Commands[ nameof( OnNext ) ];

	public bool CanExecute_OnNext() => NextButtonEnabled;

	public async void Execute_OnNext()
	{
		switch( State )
		{
		case STATE.ASSETS when OnConfirmAssets is not null:
			var UnScanned = ( from A in AssetDropOffTrips
			                  where A.IsVisible
			                  select A.DisplayTripId ).ToArray();

			if( UnScanned.Length > 0 )
			{
				var Message = string.Join( "\r\n", UnScanned );

				if( !await OnConfirmAssets( $"UNSCANNED ASSETS\r\n\n{Message}" ) )
					return;
			}
			break;

		case STATE.FINALISE:
			TripDisplayEntry? CttEntry = null;
			var               DropOffs = new List<string>();

			foreach( var Entry in TripSToProcess )
			{
				if( Entry.ServiceLevel != SCANNED_DROP_OFF )
				{
					if( Entry.IsCTT && CttEntry is null )
						CttEntry = Entry;

					var Rt = Entry.ConvertToRemoteTrip();

					var Sig = Signature!;
					Rt.sigFilename = Sig.Points;
					Rt.sigHeight   = Sig.Height;
					Rt.sigWidth    = Sig.Width;

					Rt.POD     = true;
					Rt.podName = POD;

					var Status = Entry.Status = Entry.IsDsd
					                            || Entry.IsPickedUpAsset()
					                            || Entry.IsPickedUpReturn() ? STATUS.VERIFIED : STATUS.PICKED_UP;
					Rt.status = (int)Status;

					var Now = DateTime.Now;

					if( Status == STATUS.VERIFIED )
					{
						Rt.deliveredTimeSpecified      = true;
						Rt.deliveredTime               = Now;
						Rt.deliveryArriveTimeSpecified = true;
						Rt.deliveryArriveTime          = Now;
					}
					else
					{
						Rt.pickupTimeSpecified       = true;
						Rt.pickupTime                = Now;
						Rt.pickupArriveTimeSpecified = true;
						Rt.pickupArriveTime          = Now;
					}
					TripUpdate.Update( Rt );
				}
				else
					DropOffs.Add( Entry.TripId );
			}

			ReplaceTrips( TripSToProcess );

			if( CttEntry is not null )
				TripUpdate.UpdateRbhAssets( TripDisplayEntry.TOTE_ACCOUNT_ID, CttEntry.TripId, DropOffs );
			break;
		}

		NextState();
	}
#endregion
#endregion

#region Filtering
	private ObservableCollection<TripDisplayEntry> AssetDropOffTrips  = [],
	                                               AssetReturnedTrips = [],
	                                               DsdTrips           = [],
	                                               ReturnTrips        = [];

	private readonly ObservableCollection<TripDisplayEntry> TappedReturnTrip = [];

	private ObservableCollection<TripDisplayEntry> ScannedAssets        = [],
	                                               ScannedAssetsReturns = [],
	                                               ScannedReturns       = [],
	                                               ScannedDsd           = [],
	                                               TripSToProcess       = [];

	protected override bool OnFilterTrip( TripDisplayEntry trip )
	{
		ObservableCollection<TripDisplayEntry> List;

		switch( State )
		{
		case STATE.ASSETS:
			List = AssetDropOffTrips;
			break;

		case STATE.ASSETS_RETURN:
			List = AssetReturnedTrips;
			break;

		case STATE.RETURNS:
			List = ReturnTrips;
			break;

		case STATE.DSD:
			List = DsdTrips;
			break;

		default:
			return base.OnFilterTrip( trip );
		}

		var TripIds = ( from T in List
		                select T.TripId ).ToList();

		return TripIds.Contains( trip.TripId );
	}
#endregion

#region Barcode
	public string Placeholder
	{
		get { return Get( () => Placeholder, "" ); }
		set { Set( () => Placeholder, value ); }
	}

	public Action? LocationNotFound;

	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	private async Task KillPickupDelivery()
	{
		Sounds.PlayBadScanSound();

		if( OnCanceled is not null )
			await OnCanceled();

		State = STATE.LOCATION;
	}

	private readonly HashSet<string> BoxIds = [];

	[DependsUpon( nameof( Barcode ) )]
	public async void WhenBarcodeChanges()
	{
		try
		{
			Scanning = false;

			var BCode = Barcode.Trim();

			try
			{
				if( BCode != "" )
				{
					Barcode = "";

					async Task<(bool Ok, bool IsBoxed )> BarcodeOk()
					{
						bool IsOk()
						{
							return ( BCode.Length <= 10 ) && BCode[ 0 ] is 'T' or 'L';
						}

						bool Ok,
						     IsBox = BCode.StartsWith( "BOX", StringComparison.OrdinalIgnoreCase );

						if( IsBox )
						{
							if( BoxIds.Contains( BCode ) )
							{
								await ErrorDialog.Execute( $"BOX: {BCode}\r\nAlready Scanned." );
								Ok = false;
							}
							else
								Ok = true;
						}
						else
						{
							Ok = State switch
							     {
								     STATE.ASSETS     => IsOk(),
								     STATE.ASSET_DROP => IsOk(),
								     _                => true
							     };
						}
						return ( Ok, IsBox );
					}

					async Task MatchScan( ObservableCollection<TripDisplayEntry> list, ICollection<TripDisplayEntry> scannedList, bool enableNext )
					{
						var Found = false;

						try
						{
							var (Ok, IsBox) = await BarcodeOk();

							if( Ok && !IsBox )
							{
								if( ( scannedList.Count < list.Count ) && !( from Sa in scannedList
								                                             where Sa.DisplayTripId == BCode
								                                             select Sa ).Any() )
								{
									foreach( var Entry in list )
									{
										if( Entry.DisplayTripId == BCode )
										{
											Entry.IsVisible = false;
											scannedList.Add( Entry );
											Found = true;

											NextButtonEnabled = enableNext switch
											                    {
												                    true => scannedList.Count == list.Count,
												                    _    => true
											                    };

											break;
										}
									}
								}
							}
						}
						finally
						{
							if( !Found )
								await KillPickupDelivery();
						}
					}

					switch( State )
					{
					case STATE.LOCATION:
						BoxIds.Clear();

						var Trps = Trips;

						foreach( var Trp in Trps )
							Trp.IsVisible = true;

						var LocTrips = ( from T in Trps
						                 where ( T.PickupLocation == BCode ) || ( T.DeliveryLocation == BCode )
						                 select T ).ToList();

						if( LocTrips.Count > 0 )
						{
							ReturnTrips = new ObservableCollection<TripDisplayEntry>( from T in LocTrips
							                                                          where T.IsReturn()
							                                                          select T );

							DsdTrips = new ObservableCollection<TripDisplayEntry>( from T in LocTrips
							                                                       where T.IsDsd()
							                                                       select T );

							if( Login.Required )
							{
								var AssetTrips = new List<TripDisplayEntry>( from T in LocTrips
								                                             where T.IsAsset()
								                                             select T with
								                                                    {
									                                                    IsVisible = true, // Won't copy virtual
									                                                    ShowReferenceInsteadOfTripId = true
								                                                    } );

								AssetDropOffTrips = new ObservableCollection<TripDisplayEntry>( from T in AssetTrips
								                                                                where T.IsDispatchedAsset()
								                                                                select T );

								AssetReturnedTrips = new ObservableCollection<TripDisplayEntry>( from T in AssetTrips
								                                                                 where T.IsPickedUpAsset()
								                                                                 select T );

								if( AssetDropOffTrips.Count > 0 )
								{
									State = STATE.ASSETS;
									return;
								}

								if( AssetReturnedTrips.Count > 0 )
								{
									State = STATE.ASSETS_RETURN;
									return;
								}
							}
							else
								AssetDropOffTrips = [];

							State = ReturnTrips.Count > 0 ? STATE.RETURNS : STATE.DSD;
						}
						else
							LocationNotFound?.Invoke();
						break;

					case STATE.ASSETS:
						await MatchScan( AssetDropOffTrips, ScannedAssets, false );
						break;

					case STATE.ASSETS_RETURN:
						await MatchScan( AssetReturnedTrips, ScannedAssetsReturns, true );
						break;

					case STATE.RETURNS:
						await MatchScan( ReturnTrips, ScannedReturns, true );
						break;

					case STATE.TAPPED_RETURN:
						if( TappedReturnTrip.Count > 0 )
						{
							var Return = TappedReturnTrip[ 0 ];
							ReturnTrips.Remove( Return );
							Return.Reference = BCode;
							Return.IsVisible = false;
							ScannedReturns.Add( Return );
							PreviousNextEnable = true;
						}
						NextState();
						break;

					case STATE.DSD:
						await MatchScan( DsdTrips, ScannedDsd, true );
						break;

					case STATE.ASSET_DROP:
						var (Ok, IsBox) = await BarcodeOk();

						if( Ok )
						{
							var Sd = ScannedDropOffs;

							var CttCount = ( from D in ScannedDsd
							                 where D.IsCTT
							                 select D ).Count();

							if( IsBox )
								BoxIds.Add( BCode );

							var ScanMax = ( CttCount * 2 ) - BoxIds.Count; // 2 Assets per CTT or 1 Box

							if( ( Sd.Count < ScanMax ) && !Sd.Contains( BCode ) )
								Sd.Add( BCode );
							else
								Ok = false;

							NextButtonEnabled = Ok && ( Sd.Count == ScanMax );
						}

						if( !Ok )
							await KillPickupDelivery();

						break;
					}
				}
			}
			finally
			{
				Scanning = true;
			}
		}
		catch
		{
		}
	}

	public bool Scanning

	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}

	public bool ScannerEnabled

	{
		get { return Get( () => ScannerEnabled, true ); }
		set { Set( () => ScannerEnabled, value ); }
	}
#endregion

#region State
	public STATE DisplayState
	{
		get { return Get( () => DisplayState, STATE.NONE ); }

		set { Set( () => DisplayState, value ); }
	}

	public string Title

	{
		get { return Get( () => Title, "" ); }
		set { Set( () => Title, value ); }
	}

	public enum STATE
	{
		NONE,
		WAITING_FOR_TRIPS,
		LOCATION,
		ASSETS,
		ASSETS_RETURN,
		RETURNS,
		TAPPED_RETURN,
		DSD,
		ASSET_DROP,
		SHOW_SCANNED,
		FINALISE
	}

	private void NextState()
	{
		switch( State )
		{
		case STATE.LOCATION:
			if( AssetDropOffTrips.Count > 0 )
			{
				State = STATE.ASSETS;
				break;
			}
			goto case STATE.ASSETS;

		case STATE.ASSETS:
			if( AssetReturnedTrips.Count > 0 )
			{
				State = STATE.ASSETS_RETURN;
				break;
			}
			goto case STATE.ASSETS_RETURN;

		case STATE.TAPPED_RETURN:
		case STATE.ASSETS_RETURN:
			if( ReturnTrips.Count > 0 )
			{
				State = STATE.RETURNS;
				break;
			}
			goto case STATE.RETURNS;

		case STATE.RETURNS:
			if( DsdTrips.Count > 0 )
			{
				State = STATE.DSD;
				break;
			}
			goto case STATE.DSD;

		case STATE.DSD:
			if( ( from R in DsdTrips
			      where R.IsCTT
			      select R ).Any() )
			{
				State = STATE.ASSET_DROP;
				break;
			}
			goto case STATE.ASSET_DROP;

		case STATE.ASSET_DROP:
			State = STATE.SHOW_SCANNED;
			break;

		case STATE.SHOW_SCANNED:
			State = STATE.FINALISE;
			break;

		case STATE.FINALISE:
			State = STATE.WAITING_FOR_TRIPS;
			break;
		}
	}

	private STATE _State = STATE.NONE;

	public bool StateIsTappedInput

	{
		get { return Get( () => StateIsTappedInput, false ); }
		set { Set( () => StateIsTappedInput, value ); }
	}

	private bool PreviousNextEnable;

	private STATE State
	{
		get => _State;

		set
		{
			_State             = value;
			StateIsTappedInput = value == STATE.TAPPED_RETURN;

			switch( value )
			{
			case STATE.NONE:
			case STATE.WAITING_FOR_TRIPS:
				WaitingForTrips = true;
				break;

			case STATE.LOCATION:
				Deliveries = true;

				RefreshTrips();

				var Trps = ( from T in Trips
				             where TripFilterCallback( T )
				             orderby T.Status, T.TripId
				             select T ).ToList();

				foreach( var Trip in Trps ) // From Cancel
				{
					Trip.IsVisible                    = true;
					Trip.ShowReferenceInsteadOfTripId = Trip.IsAsset || Trip.IsReturn;
				}

				DisplayTrips = new ObservableCollection<TripDisplayEntry>( Trps );

				Placeholder          = Globals.Resources.GetStringResource( "Location" );
				Title                = Globals.Resources.GetStringResource( "ScanLocation" );
				AssetDropOffTrips    = [];
				AssetReturnedTrips   = [];
				DsdTrips             = [];
				ReturnTrips          = [];
				ScannedAssets        = [];
				ScannedAssetsReturns = [];
				ScannedReturns       = [];
				ScannedDsd           = [];
				ScannedDropOffs      = [];
				TripsVisible         = true;
				ShowScanner          = true;
				ScannerEnabled       = true;
				PreviousNextEnable   = false;
				NextButtonVisible    = false;
				PodSignVisible       = false;
				POD                  = "";
				Signature            = null;
				break;

			case STATE.ASSETS:
				DisplayTrips      = AssetDropOffTrips;
				Placeholder       = Globals.Resources.GetStringResource( "ScanAsset" );
				Title             = Globals.Resources.GetStringResource( "PickupToteAsset" );
				NextButtonText    = Globals.Resources.GetStringResource( "AcceptAssets" );
				NextButtonEnabled = true;
				NextButtonVisible = true;
				break;

			case STATE.ASSETS_RETURN:
				DisplayTrips      = AssetReturnedTrips;
				Placeholder       = Globals.Resources.GetStringResource( "ScanReturnAsset" );
				Title             = Globals.Resources.GetStringResource( "ReturnToteAsset" );
				NextButtonText    = Globals.Resources.GetStringResource( "AcceptReturnAssets" );
				NextButtonEnabled = false;
				NextButtonVisible = true;
				break;

			case STATE.TAPPED_RETURN:
				DisplayTrips       = TappedReturnTrip;
				Placeholder        = Globals.Resources.GetStringResource( "ScanTappedReturn" );
				PreviousNextEnable = NextButtonEnabled;
				goto ReturnTrips;

			case STATE.RETURNS:
				DisplayTrips = ReturnTrips;
				Placeholder  = Globals.Resources.GetStringResource( "ScanReturns" );
			ReturnTrips:
				Title              = Globals.Resources.GetStringResource( "ReturnTotes" );
				NextButtonText     = Globals.Resources.GetStringResource( "Accept Returns" );
				NextButtonEnabled  = PreviousNextEnable;
				PreviousNextEnable = false;
				NextButtonVisible  = true;
				break;

			case STATE.DSD:
				DisplayTrips      = DsdTrips;
				Placeholder       = Globals.Resources.GetStringResource( "ScanTote" );
				Title             = Globals.Resources.GetStringResource( "DsdDeliveries" );
				NextButtonText    = Globals.Resources.GetStringResource( "AcceptDeliveries" );
				NextButtonEnabled = false;
				NextButtonVisible = true;
				break;

			case STATE.ASSET_DROP:
				TripsVisible      = false;
				Placeholder       = Globals.Resources.GetStringResource( "ScanAssetTotes" );
				Title             = Globals.Resources.GetStringResource( "AssetDropOffs" );
				NextButtonText    = Globals.Resources.GetStringResource( "AcceptPickupAssets" );
				NextButtonEnabled = false;
				NextButtonVisible = true;
				break;

			case STATE.SHOW_SCANNED:
				Scanning       = false;
				ScannerEnabled = false;
				Title          = Globals.Resources.GetStringResource( "ConfirmScannedTotes" );
				ShowScanner    = false;
				TripsVisible   = true;
				var Temp = new List<TripDisplayEntry>();

				void AppendList( IEnumerable<TripDisplayEntry> list )
				{
					Temp.AddRange( from Entry in list
					               select Entry with
					                      {
						                      IsVisible = true
					                      } );
				}

				AppendList( ScannedAssets );
				AppendList( ScannedAssetsReturns );
				AppendList( ScannedReturns );
				AppendList( ScannedDsd );

				AppendList( from ScannedDropOff in ScannedDropOffs
				            where !ScannedDropOff.StartsWith( "BOX", StringComparison.OrdinalIgnoreCase )
				            select new TripDisplayEntry
				                   {
					                   TripId       = ScannedDropOff,
					                   Status       = STATUS.DISPATCHED,
					                   ServiceLevel = SCANNED_DROP_OFF
				                   } );

				DisplayTrips   = TripSToProcess = new ObservableCollection<TripDisplayEntry>( Temp );
				NextButtonText = Globals.Resources.GetStringResource( "Ok" );
				break;

			case STATE.FINALISE:
				PodSignVisible    = true;
				NextButtonEnabled = false;
				NextButtonVisible = true;
				break;
			}
			RefreshTrips();
			Scanning      = true;
			Keyboard.Show = false;
		}
	}
#endregion

#region Waiting For Trips
	public Action<bool>? OnWaitingForTrips;

	public bool WaitingForTrips

	{
		get { return Get( () => WaitingForTrips, true ); }
		set { Set( () => WaitingForTrips, value ); }
	}

	[DependsUpon( nameof( WaitingForTrips ) )]
	public void WhenWaitingForTripsChanges()
	{
		var Waiting = WaitingForTrips;
		OnWaitingForTrips?.Invoke( Waiting );

		if( !Waiting )
			State = STATE.LOCATION;
	}
#endregion

#region Tapped Return
	public ICommand ReturnTapped => Commands[ nameof( ReturnTapped ) ];

	public void Execute_ReturnTapped( TripDisplayEntry? item )
	{
		if( item is not null && ( State == STATE.RETURNS ) )
		{
			TappedReturnTrip.Clear();
			TappedReturnTrip.Add( item );
			State = STATE.TAPPED_RETURN;
		}
	}
#endregion
}