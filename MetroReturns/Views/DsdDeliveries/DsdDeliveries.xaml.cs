﻿using System.Globalization;
using Keyboard = Globals.Keyboard;

namespace MetroReturns.Views;

public class StateToBackgroundColourConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is DsdDeliveriesViewModel.STATE State )
		{
			switch( State )
			{
			case DsdDeliveriesViewModel.STATE.ASSETS:
				return Color.DarkOrange;
			}
		}
		return Color.Black;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

public class StateToTextColourConvertor : IValueConverter
{
	public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
	{
		if( value is DsdDeliveriesViewModel.STATE State )
		{
			switch( State )
			{
			case DsdDeliveriesViewModel.STATE.ASSETS:
				return Color.Black;
			}
		}
		return Color.White;
	}

	public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) => throw new NotImplementedException();
}

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class DsdDeliveries : ContentView
{
	public DsdDeliveries()
	{
		InitializeComponent();
		var M = (DsdDeliveriesViewModel)Root.BindingContext;

		M.OnWaitingForTrips = waiting =>
		                      {
			                      ViewModelBase.Dispatcher( () =>
			                                                {
				                                                Root.RaiseChild( waiting ? WaitingForTrips : Deliveries );
			                                                } );
		                      };

		M.OnCanceled = async () =>
		               {
			               await CanceledDialog.Execute( "ScanErrorPickupDelivery" );
		               };

		M.OnConfirmCancel = async () => await ConfirmDialog.Execute( "CancelPickupDelivery" );

		M.OnWaitingForTrips( true );

		M.LocationNotFound = async () =>
		                     {
			                     await ErrorDialog.Execute( "NotForThisLocation" );
		                     };

		M.OnConfirmAssets = async message => await ConfirmDialog.Execute( message );

		Signature.OnOk = signature =>
		                 {
			                 Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT;
			                 M.OnSignatureComplete( signature );
		                 };

		Signature.OnCancel = () =>
		                     {
			                     Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT;
			                     M.OnSignatureComplete( null );
		                     };

		M.OnShowSignature = () =>
		                    {
			                    Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
			                    Signature.Clear();
			                    Keyboard.Show = false;
		                    };

		WaitingForTrips.TripFilterCallback = M.TripFilterCallback;
	}
}