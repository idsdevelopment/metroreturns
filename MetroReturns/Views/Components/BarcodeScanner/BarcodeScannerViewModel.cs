﻿using Keyboard = Globals.Keyboard;

namespace MetroReturns.Components;

public class BarcodeScannerViewModel : ViewModelBase
{
	public BarcodeScanner Owner = null!;

#region Fifo
	private void UpdateFifo( string barcode )
	{
		static void ClearFifo()
		{
			lock( BarcodeFifo )
				BarcodeFifo.Clear();
		}

		Keyboard.Show = false;

		lock( BarcodeFifo )
		{
			BarcodeFifo.AddLast( barcode );

			if( BarcodeFifo.Count == 1 ) // Need to start unload task
			{
				Tasks.RunVoid( () =>
				               {
					               try
					               {
						               while( true )
						               {
							               string BCode;

							               lock( BarcodeFifo )
							               {
								               if( BarcodeFifo.Count == 0 )
									               return;

								               BCode = BarcodeFifo.First.Value;
								               BarcodeFifo.RemoveFirst();
							               }

							               Dispatcher( () =>
							                           {
								                           Owner.Barcode = BCode;
							                           } );
						               }
					               }
					               finally
					               {
						               ClearFifo();
					               }
				               } );
			}
		}
	}
#endregion

#region Buttons
	public ICommand OnAccept => Commands[ nameof( OnAccept ) ];

	public void Execute_OnAccept()
	{
		UpdateFifo();
	}
#endregion


#region ZXing
	public Func<Task<string>>? ExecuteZXingScanner;

	public ICommand ZXingScanner => Commands[ nameof( ZXingScanner ) ];

	public async void Execute_ZXingScanner()
	{
		if( ExecuteZXingScanner is not null && Scanning )
			Barcode = await ExecuteZXingScanner.Invoke();
	}
#endregion

#region Scanning
	private bool InScanningChange;

	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}

	[DependsUpon( nameof( Scanning ) )]
	public void WhenScanningChanges()
	{
		if( !InScanningChange )
		{
			InScanningChange = true;
			var Scn = Scanning;
			DisableScanning  = !Scn;
			Owner.Scanning   = Scn;
			InScanningChange = false;
		}
	}

	public bool DisableScanning
	{
		get { return Get( () => DisableScanning, true ); }
		set { Set( () => DisableScanning, value ); }
	}
#endregion

#region Barcode
	private static readonly LinkedList<string> BarcodeFifo = [];

	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	[DependsUpon250( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		UpdateFifo();
	}

	private void UpdateFifo()
	{
		var BCode = Barcode.Trim();

		if( BCode.IsNotNullOrWhiteSpace() )
			UpdateFifo( BCode );

		Barcode = "";
	}
#endregion
}