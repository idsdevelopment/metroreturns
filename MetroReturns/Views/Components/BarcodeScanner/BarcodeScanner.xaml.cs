﻿using System.Diagnostics;
using BarcodeInterface;
using ZXing.Mobile;
using Keyboard = Globals.Keyboard;
using Timer = System.Timers.Timer;

namespace MetroReturns.Components;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class BarcodeScanner : ContentView, IBarcodeScanner
{
	private static MobileBarcodeScanner? _Scanner;
	private static MobileBarcodeScanner  Scanner => _Scanner ??= new MobileBarcodeScanner();

	private readonly BarcodeScannerViewModel Model;

	public BarcodeScanner()
	{
		InitializeComponent();

		var M = (BarcodeScannerViewModel)Root.BindingContext;
		Model   = M;
		M.Owner = this;

		M.ExecuteZXingScanner = async () =>
		                        {
			                        var Tm = new Timer( 5_000 )
			                                 {
				                                 AutoReset = true
			                                 };

			                        Tm.Elapsed += ( _, _ ) =>
			                                      {
				                                      ViewModelBase.Dispatcher( () =>
				                                                                {
					                                                                Scanner.AutoFocus();
				                                                                } );
			                                      };

			                        try
			                        {
				                        Tm.Start();

				                        return ( await Scanner.Scan( new MobileBarcodeScanningOptions
				                                                     {
					                                                     AutoRotate       = true,
					                                                     DisableAutofocus = false,
					                                                     TryHarder        = true
				                                                     } ) )?.Text ?? "";
			                        }
			                        catch( Exception Exception )
			                        {
				                        Debug.WriteLine( Exception );
				                        return "";
			                        }
			                        finally
			                        {
				                        Tm.Dispose();
			                        }
		                        };

		BarcodeEntry.Unfocused += ( _, _ ) =>
		                          {
			                          FocusDelayed();
		                          };

		BarcodeEntry.Focused += ( _, _ ) =>
		                        {
			                        Keyboard.Show = false;
		                        };
	}

#region Focus
	private void FocusEntry()
	{
		if( !BarcodeEntry.IsFocused )
			BarcodeEntry.Focus();

		Keyboard.Show = false;
	}

	private void FocusDelayed( int delay = 350 )
	{
		if( IsEnabled && !BarcodeEntry.IsFocused )
		{
			Tasks.RunVoid( delay, () =>
			                      {
				                      ViewModelBase.Dispatcher( FocusEntry );
			                      } );
		}
	}

	bool IBarcodeScanner.Focus()
	{
		FocusEntry();
		return true;
	}

	public new bool Focus()
	{
		FocusEntry();
		return true;
	}
#endregion

#region Enabled
	private static void IsEnabledPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is bool IsEnabled )
		{
			Bs.Model.DisableScanning      = !IsEnabled;
			( (ContentView)Bs ).IsEnabled = IsEnabled;
			var Entry = Bs.BarcodeEntry;
			Entry.IsEnabled = IsEnabled;

			if( IsEnabled )
				Entry.Focus();
		}
	}

	public new static readonly BindableProperty IsEnabledProperty = BindableProperty.Create( nameof( IsVisible ),
	                                                                                         typeof( bool ),
	                                                                                         typeof( BarcodeScanner ),
	                                                                                         false,
	                                                                                         BindingMode.TwoWay,
	                                                                                         propertyChanged: IsEnabledPropertyChanged );

	public new bool IsEnabled
	{
		get => (bool)GetValue( IsEnabledProperty );
		set => SetValue( IsEnabledProperty, value );
	}
#endregion

#region Interface
#region Scanning
	private bool InScanningChange;

	private static void ScanningPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner {InScanningChange: false} Bs && newValue is bool Scanning )
		{
			Bs.InScanningChange = true;

			if( Scanning )
				Bs.IsEnabled = true;

			Bs.Model.Scanning = Scanning;

			if( Scanning )
				Bs.FocusDelayed();

			Bs.InScanningChange = false;
		}
	}


	public static readonly BindableProperty ScanningProperty = BindableProperty.Create( nameof( Scanning ),
	                                                                                    typeof( bool ),
	                                                                                    typeof( BarcodeScanner ),
	                                                                                    false,
	                                                                                    BindingMode.TwoWay,
	                                                                                    propertyChanged: ScanningPropertyChanged );

	public bool Scanning
	{
		get => (bool)GetValue( ScanningProperty );
		set => SetValue( ScanningProperty, value );
	}
#endregion

#region Barcode
	public static readonly BindableProperty BarcodeProperty = BindableProperty.Create( nameof( Barcode ),
	                                                                                   typeof( string ),
	                                                                                   typeof( BarcodeScanner ),
	                                                                                   "",
	                                                                                   BindingMode.TwoWay
	                                                                                 );

	public string Barcode
	{
		get => (string)GetValue( BarcodeProperty );
		set => SetValue( BarcodeProperty, value );
	}
#endregion

#region Placeholder
	public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create( nameof( Placeholder ),
	                                                                                       typeof( string ),
	                                                                                       typeof( BarcodeScanner ),
	                                                                                       "",
	                                                                                       propertyChanged: PlaceholderPropertyChanged
	                                                                                     );

	private static void PlaceholderPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is string Holder )
			Bs.BarcodeEntry.Placeholder = Holder;
	}


	public string Placeholder
	{
		get => (string)GetValue( PlaceholderProperty );
		set => SetValue( PlaceholderProperty, value );
	}
#endregion


#region PlaceholderTextColor
	public static readonly BindableProperty PlaceholderTextColorProperty = BindableProperty.Create( nameof( PlaceholderTextColor ),
	                                                                                                typeof( Color ),
	                                                                                                typeof( BarcodeScanner ),
	                                                                                                Color.Black,
	                                                                                                propertyChanged: PlaceholderTextColorPropertyChanged
	                                                                                              );

	private static void PlaceholderTextColorPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is BarcodeScanner Bs && newValue is Color TextColor )
			Bs.BarcodeEntry.PlaceholderColor = TextColor;
	}


	public Color PlaceholderTextColor
	{
		get => (Color)GetValue( PlaceholderTextColorProperty );
		set => SetValue( PlaceholderTextColorProperty, value );
	}

	public Action<string>? OnBarcode { get; set; } // Interface
#endregion

	public ContentView ScannerView
	{
		get => this;
		set { }
	}

	public void Dispose()
	{
	}
#endregion
}