﻿namespace BarcodeInterface;

public interface IBarcodeScanner
{
	bool   Scanning  { get; set; }
	bool   IsEnabled { get; set; }
	string Barcode   { get; }

	string Placeholder          { get; set; }
	Color  PlaceholderTextColor { get; set; }

	Action<string>? OnBarcode { get; set; }

	ContentView ScannerView { get; set; }

	bool Focus();
}