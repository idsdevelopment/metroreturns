﻿namespace MetroReturns.Components;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class WaitingForTrips : ContentView
{
	private readonly WaitingFotTripsViewModel Model;

	public WaitingForTrips()
	{
		InitializeComponent();

		var M = Model = (WaitingFotTripsViewModel)Root.BindingContext;

		M.OnCancelAction = () =>
		                   {
			                   var Oc = OnCancel;

			                   if( Oc is not null && Oc.CanExecute( this ) )
			                   {
				                   IsEnabled = false;
				                   Oc.Execute( this );
			                   }
		                   };

		M.OnEnable = enabled =>
		             {
			             IsEnabled = enabled;
		             };

		M.Enabled = true;
	}

#region Status Filter
	private static void StatusFilterPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is WaitingForTrips Wft && newValue is Service.STATUS[] Filter )
			Wft.Model.StatusFilter = Filter;
	}

	public static readonly BindableProperty StatusFilterProperty = BindableProperty.Create( nameof( StatusFilter ),
	                                                                                        typeof( Service.STATUS[] ),
	                                                                                        typeof( WaitingForTrips ),
	                                                                                        Array.Empty<Service.STATUS>(),
	                                                                                        propertyChanged: StatusFilterPropertyChanged );

	public Service.STATUS[] StatusFilter
	{
		get => (Service.STATUS[])GetValue( StatusFilterProperty );
		set => SetValue( StatusFilterProperty, value );
	}
#endregion

#region Trip Filter
	private static void TripFilterCallbackPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is WaitingForTrips Wft && newValue is Func<TripDisplayEntry, bool> Filter )
			Wft.Model.OnFilterTripCallback = Filter;
	}

	public static readonly BindableProperty TripFilterCallbackProperty = BindableProperty.Create( nameof( TripFilterCallback ),
	                                                                                              typeof( Func<TripDisplayEntry, bool> ),
	                                                                                              typeof( WaitingForTrips ),
	                                                                                              propertyChanged: TripFilterCallbackPropertyChanged );

	public Func<TripDisplayEntry, bool> TripFilterCallback
	{
		get => (Func<TripDisplayEntry, bool>)GetValue( TripFilterCallbackProperty );
		set => SetValue( TripFilterCallbackProperty, value );
	}
#endregion


#region On Cancel
	public static readonly BindableProperty OnCancelProperty = BindableProperty.Create( nameof( OnCancel ),
	                                                                                    typeof( ICommand ),
	                                                                                    typeof( WaitingForTrips )
	                                                                                  );

	public ICommand? OnCancel
	{
		get => (ICommand?)GetValue( OnCancelProperty );
		set => SetValue( OnCancelProperty, value );
	}
#endregion

#region Enabled
	private static void IsEnabledPropertyChanged( BindableObject bindable, object oldValue, object newValue )
	{
		if( bindable is WaitingForTrips Wft && newValue is bool IsEnabled )
		{
			Wft.ContentView.IsVisible = IsEnabled;
			Wft.Model.Enabled         = IsEnabled;
		}
	}

	public new static readonly BindableProperty IsEnabledProperty = BindableProperty.Create( nameof( IsEnabled ),
	                                                                                         typeof( bool ),
	                                                                                         typeof( WaitingForTrips ),
	                                                                                         false,
	                                                                                         BindingMode.TwoWay,
	                                                                                         propertyChanged: IsEnabledPropertyChanged );

	public new bool IsEnabled
	{
		get => (bool)GetValue( IsEnabledProperty );
		set => SetValue( IsEnabledProperty, value );
	}
#endregion
}