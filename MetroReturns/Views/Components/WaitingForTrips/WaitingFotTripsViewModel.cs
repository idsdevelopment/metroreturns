﻿namespace MetroReturns.Components;

internal class WaitingFotTripsViewModel : TripsViewModelBase
{
	public new bool Enabled
	{
		get { return Get( () => Enabled, false ); }
		set { Set( () => Enabled, value ); }
	}

	public Action<bool>? OnEnable;

	[DependsUpon( nameof( Enabled ) )]
	public void WhenEnabledChanges()
	{
		var Enab = Enabled;

		if( Enab )
		{
			WaitForTrips( waiting =>
			              {
				              if( !waiting )
					              Enabled = false;
			              } );
		}
		else
			base.Enabled = false;

		OnEnable?.Invoke( Enab );
	}

#region Cancel
	public Action?  OnCancelAction;
	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public bool CanExecute_OnCancel()
	{
		return true;
	}

	public void Execute_OnCancel()
	{
		Enabled = false;
		OnCancelAction?.Invoke();
	}
#endregion
}