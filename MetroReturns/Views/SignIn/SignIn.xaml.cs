﻿namespace MetroReturns.Views.SignIn;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class SignIn : ContentView
{
	public SignIn()
	{
		InitializeComponent();

		Tasks.RunVoid( 200, () =>
		                    {
			                    Dispatcher.BeginInvokeOnMainThread( () =>
			                                                        {
				                                                        if( UserName.Text.Trim() == "" )
					                                                        UserName.Focus();
				                                                        else
					                                                        Password.Focus();
			                                                        } );
		                    } );

		var M = (SignInViewModel)Root.BindingContext;

		M.OnSignInError = async () =>
		                  {
			                  await ErrorDialog.Execute( "InvalidCredentials" );
		                  };
	}
}