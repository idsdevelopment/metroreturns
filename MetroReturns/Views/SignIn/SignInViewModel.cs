﻿namespace MetroReturns.Views.SignIn;

internal class SignInViewModel : ViewModelBase
{
	protected override void OnInitialised()
	{
		base.OnInitialised();
		WhenLanguageChanges();
	}

	public bool EnableSignIn
	{
		get { return Get( () => EnableSignIn, false ); }
		set { Set( () => EnableSignIn, value ); }
	}

	[Setting]
	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}


	public string Password
	{
		get { return Get( () => Password, "" ); }
		set { Set( () => Password, value ); }
	}


	public ICommand SignInButtonClick => Commands[ nameof( SignInButtonClick ) ];
	public Action   OnSignInError = null!;

	[DependsUpon250( nameof( UserName ) )]
	[DependsUpon250( nameof( Password ) )]
	public void WhenSignInChanges()
	{
		EnableSignIn = ( Password != "" ) && ( UserName.Trim() != "" );
	}

	public bool CanExecute_SignInButtonClick() => EnableSignIn;

	public async void Execute_SignInButtonClick()
	{
		try
		{
			var (Ok, UName, Required) = await Service.Client.LoginAsync( UserName, Password );

			if( !Ok )
				OnSignInError();
			else
			{
				Login.Required = Required;

				SaveSettings();
				MainPageViewModel.Instance.SignedIn = Login.SignedIn = true;
				Login.UserName                      = UName;
			}
		}
		catch
		{
		}
	}

#region Language
	[Setting]
	public bool IsEnglish
	{
		get { return Get( () => IsEnglish, true ); }
		set { Set( () => IsEnglish, value ); }
	}


	[Setting]
	public bool IsFrench
	{
		get { return Get( () => IsFrench, false ); }
		set { Set( () => IsFrench, value ); }
	}

	[DependsUpon( nameof( IsEnglish ) )]
	[DependsUpon( nameof( IsFrench ) )]
	public void WhenLanguageChanges()
	{
		App.Language = IsEnglish ? App.LANGUAGE.ENGLISH : App.LANGUAGE.FRENCH;
		SaveSettings();
	}
#endregion
}