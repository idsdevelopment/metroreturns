﻿using System.Timers;
using AppUpdater;
using Timer = System.Timers.Timer;

namespace MetroReturns.Views;

internal class MainPageViewModel : ViewModelBase
{
	public enum STATE
	{
		NO_INTERNET,
		INTERNET,
		SIGN_IN,
		RETURNS,
		HOME,
		DRIVER_VERIFICATION,
		DSD_DELIVERIES,
		DRIVER_CLAIMS,
		WAREHOUSE_CLAIMS
	}

	public static MainPageViewModel Instance { get; private set; } = null!;

	public Action InitUpdates = null!;

	public Action<STATE> Show = null!;

	protected override void OnInitialised()
	{
		Instance = this;

		base.OnInitialised();

		PingTimer = new Timer( 5_000 )
		            {
			            AutoReset = true
		            };

		PingTimer.Elapsed += OnPingTimerOnElapsed;
		PingTimer.Start();
		OnPingTimerOnElapsed( this, null );
	}

	public MainPageViewModel()
	{
		Instance = this;
	}


#region Signed In
	public bool SignedIn
	{
		get { return Get( () => SignedIn, false ); }
		set { Set( () => SignedIn, value ); }
	}

	public bool NotSignedIn
	{
		get { return Get( () => NotSignedIn, true ); }
		set { Set( () => NotSignedIn, value ); }
	}


	private bool InSignInChange;

	[DependsUpon( nameof( SignedIn ) )]
	public void WhenSignInChanged()
	{
		if( !InSignInChange )
		{
			InSignInChange = true;
			var Sin = SignedIn;
			NotSignedIn = !Sin;

			if( Sin )
			{
				Show( STATE.HOME );
				InitUpdates();
			}
			InSignInChange = false;
		}
	}

	[DependsUpon( nameof( NotSignedIn ) )]
	public void WhenNotSignInChanged()
	{
		if( !InSignInChange )
		{
			InSignInChange = true;
			SignedIn       = !NotSignedIn;
			InSignInChange = false;
		}
	}
#endregion


#region Update Available
	public string CurrentVersion
	{
		get { return Get( () => CurrentVersion, MetroReturns.CurrentVersion.VERSION_NUMBER ); }
		set { Set( () => CurrentVersion, value ); }
	}


	public bool UpdateAvailable
	{
		get { return Get( () => UpdateAvailable, false ); }
		set { Set( () => UpdateAvailable, value ); }
	}


	public bool UpdateNotAvailable
	{
		get { return Get( () => UpdateNotAvailable, true ); }
		set { Set( () => UpdateNotAvailable, value ); }
	}

	public ICommand OnUpdateAvailableButtonClick => Commands[ nameof( OnUpdateAvailableButtonClick ) ];

	public bool CanExecute_OnUpdateAvailableButtonClick() => true;

	public void Execute_OnUpdateAvailableButtonClick()
	{
		Updates.RunInstall();
	}
#endregion

#region Ping
	private Timer PingTimer = null!;

	public bool OnLine
	{
		get { return Get( () => OnLine, false ); }
		set { Set( () => OnLine, value ); }
	}


	public bool OffLine
	{
		get { return Get( () => OffLine, true ); }
		set { Set( () => OffLine, value ); }
	}


	private async void OnPingTimerOnElapsed( object sender, ElapsedEventArgs? elapsedEventArgs )
	{
		var On = await Service.Client.PingAsync();

		if( On != OnLine )
		{
			OnLine  = On;
			OffLine = !On;

			Dispatcher( () =>
			            {
				            Show( On ? STATE.INTERNET : STATE.NO_INTERNET );
			            } );
		}

		Events.OnPingEvent.Value = On;
	}
#endregion
}