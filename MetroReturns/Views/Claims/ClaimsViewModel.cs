﻿namespace MetroReturns.Views.Claims;

public class TripDisplay : INotifyPropertyChanged
{
	public string TripId => OriginalTrip.tripId;

	public string? DeliveryAddress
	{
		get => _DeliveryAddress ?? ( OfflineText ??= Globals.Resources.GetStringResource( "Offline" ) );
		set
		{
			_DeliveryAddress = value;
			OnPropertyChanged();
		}
	}

	public bool IsOffline => _DeliveryAddress is null;

	public         remoteTrip OriginalTrip { get; }
	private static string?    OfflineText;

	private string? _DeliveryAddress;

	public TripDisplay( remoteTrip trip )
	{
		OriginalTrip = trip;
	}

	[NotifyPropertyChangedInvocator]
	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	public event PropertyChangedEventHandler? PropertyChanged;
}

public class ClaimsViewModel : TripsViewModelBase
{
	public bool IsScanning
	{
		get { return Get( () => IsScanning, false ); }
		set { Set( () => IsScanning, value ); }
	}

	public int TotalScanned
	{
		get { return Get( () => TotalScanned, 0 ); }
		set { Set( () => TotalScanned, value ); }
	}

	public ObservableCollection<TripDisplay> ClaimedTrips
	{
		get { return Get( () => ClaimedTrips, [] ); }
		set { Set( () => ClaimedTrips, value ); }
	}

	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public ICommand OnClaim => Commands[ nameof( OnClaim ) ];

	protected override void OnInitialised()
	{
		base.OnInitialised();
		IsScanning = true;
	}

	public bool CanExecute_OnCancel() => true;

	public async void Execute_OnCancel()
	{
		if( await ConfirmDialog.Execute( "Cancel Claim?" ) )
			MainPage.Home();
	}

	public bool CanExecute_OnClaim() => true;

	public async void Execute_OnClaim()
	{
		if( await ConfirmDialog.Execute( "Claim Shipments Now?" ) )
		{
			var Driver     = Login.UserName;
			var PickupTime = DateTimeOffset.Now.AsPacificStandardTime();

			foreach( var Trip in ClaimedTrips )
			{
				var Ot = Trip.OriginalTrip;
				Ot.driver              = Driver;
				Ot.status              = (int)Service.STATUS.PICKED_UP;
				Ot.pickupTime          = PickupTime.DateTime;
				Ot.pickupTimeSpecified = true;
				TripUpdate.Update( Ot );
			}
			MainPage.Home();
		}
	}

#region TripId
	public string TripId
	{
		get { return Get( () => TripId, "" ); }
		set { Set( () => TripId, value ); }
	}

	[DependsUpon( nameof( TripId ) )]
	public void WhenTripIdChanges()
	{
		var Tid = TripId;

		if( Tid != "" )
		{
			var Id = Tid;

			Tasks.RunVoid( () =>
			               {
				               if( Service.Client.FindByTripIdOrReference( Id ) is { } Trip && ( Trip.IsCWD() || Trip.IsReturn() ) )
				               {
					               var Ra      = Service.Client.FindAddress( Trip.deliveryAddressId );
					               var Address = Trip.deliveryCompany;

					               if( Ra is not null )
					               {
						               Address = $"{Address}\r\n";

						               if( Ra is { suite: { } Suite and not "" } )
							               Address = $"{Suite} / ";

						               Address = $"{Address}{Ra.street.Trim()}";
					               }

					               var Ct = ClaimedTrips;

					               Ct.Add( new TripDisplay( Trip )
					                       {
						                       DeliveryAddress = Address
					                       } );

					               TotalScanned = Ct.Count;
				               }
				               else
				               {
					               Dispatcher( async () =>
					                           {
						                           await ErrorDialog.Execute( $"Shipment Id \"{Id}\"\r\nNot Found" );
					                           } );
				               }
			               } );
			TripId = "";
		}
	}
#endregion
}