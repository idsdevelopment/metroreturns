﻿// ReSharper disable InconsistentNaming

using System.Diagnostics;

namespace MetroReturns.Views.Warehouse;

internal class WarehouseClaimsViewModel : ViewModelBase
{
#region Scanning
	public bool BarcodeScannerEnabled
	{
		get { return Get( () => BarcodeScannerEnabled, false ); }
		set { Set( () => BarcodeScannerEnabled, value ); }
	}
#endregion

	public Action<bool> ShowScanner = null!;

	protected override void OnInitialised()
	{
		base.OnInitialised();
		ScanAcceptEnable = false;
	}

#region Scan Count
	private int _ScanCount;

	public int ScanCount
	{
		get => _ScanCount;
		set
		{
			_ScanCount       = value;
			DisplayScanCount = value.ToString();
		}
	}


	public string DisplayScanCount
	{
		get { return Get( () => DisplayScanCount, "0" ); }
		set { Set( () => DisplayScanCount, value ); }
	}
#endregion

#region TripDisplayEntries
	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}

	[DependsUpon( nameof( TripDisplayEntries ) )]
	public void WhenTripDisplayEntriesChanges()
	{
		var C = TripDisplayEntries.Count;
		ScanCount        = C;
		ScanAcceptEnable = C > 0;
	}
#endregion

#region Reference Id
	public Action<string> OnAlreadyScanned   = null!,
	                      OnNotPickedUp      = null!,
	                      OnAlreadyProcessed = null!;

	public Action OnBadReferenceId   = null!,
	              OnNoShipmentsFound = null!,
	              OnFocusReturnId    = null!;

	public string Reference
	{
		get { return Get( () => Reference, "" ); }
		set { Set( () => Reference, value ); }
	}

	private readonly SemaphoreSlim UpdateLock = new( 1 );
	private          bool          InReferenceChange;

	[DependsUpon150( nameof( Reference ) )]
	public void WhenReferenceChanges()
	{
		if( !InReferenceChange )
		{
			InReferenceChange = true;

			try
			{
				var Ref = Reference.Trim();

				if( Ref != "" )
				{
					Reference = "";

					if( Ref.IsInteger() )
					{
						var AlreadyScanned = ( from E in TripDisplayEntries
						                       where E.Reference == Ref
						                       select true ).FirstOrDefault();

						if( !AlreadyScanned )
						{
							Tasks.RunVoid( async () =>
							               {
								               var Trps = await Service.Client.FindTripsReferenceAsync( Ref );

								               var Trips = ( from T in Trps
								                             where T.IsCWD() || T.IsReturn()
								                             select T ).ToList();

								               foreach( var trip in Trips )
								               {
									               switch( (Service.STATUS)trip.status )
									               {
									               case Service.STATUS.PICKED_UP:
										               break;

									               case Service.STATUS.ACTIVE:
									               case Service.STATUS.DISPATCHED:
										               Dispatcher( () =>
										                           {
											                           OnNotPickedUp( Ref );
										                           } );
										               return;

									               default:
										               Dispatcher( () =>
										                           {
											                           OnAlreadyProcessed( Ref );
										                           } );
										               return;
									               }
								               }

								               await UpdateLock.WaitAsync();

								               try
								               {
									               if( Trips.Count > 0 )
									               {
										               // Stop weird screen update
										               var NewList = new ObservableCollection<TripDisplayEntry>( TripDisplayEntries );
										               var Entries = new ObservableCollection<TripDisplayEntry>( await Trips.Convert() );

										               foreach( var Entry in Entries )
											               NewList.Add( new TripDisplayEntry( Entry ) );

										               Dispatcher( () =>
										                           {
											                           TripDisplayEntries = NewList;
										                           } );
									               }
									               else
										               Dispatcher( OnNoShipmentsFound );
								               }
								               finally
								               {
									               UpdateLock.Release();
								               }
							               } );
						}
						else
							OnAlreadyScanned( Ref );
					}
					else
						OnBadReferenceId();
				}
			}
			catch( Exception Exception )
			{
				Debug.WriteLine( Exception );
			}
			finally
			{
				InReferenceChange = false;
			}
		}
	}
#endregion

#region Cancel Button
	public ICommand OnCancelButtonClick => Commands[ nameof( OnCancelButtonClick ) ];

	public void Execute_OnCancelButtonClick()
	{
		CancelButtonText       = Globals.Resources.GetStringResource( "Exit" );
		TripDisplayEntries     = [];
		Reference              = "";
		POD                    = "";
		Signature              = null;
		PodAndSignAcceptEnable = false;
		BarcodeScannerEnabled  = true;
		OnShowPodAndSignature( false );
		OnFocusReturnId();
	}
#endregion

#region Finalise
	public bool ScanAcceptEnable
	{
		get { return Get( () => ScanAcceptEnable, true ); }
		set { Set( () => ScanAcceptEnable, value ); }
	}

	[DependsUpon( nameof( ScanAcceptEnable ) )]
	public void WhenScanAcceptEnableChanges()
	{
		CancelButtonText = Globals.Resources.GetStringResource( ScanAcceptEnable ? "Cancel" : "Exit" );
	}

	public Func<Task<bool>> OnConfirmFinalise = null!;

	public ICommand OnAcceptButtonClick => Commands[ nameof( OnAcceptButtonClick ) ];

	public bool CanExecute_OnAcceptButtonClick() => ScanAcceptEnable;

	public async void Execute_OnAcceptButtonClick()
	{
		try
		{
			if( await OnConfirmFinalise() )
				DoPodAndSign();
		}
		catch
		{
		}
	}
#endregion

#region Cancel Scan
	public string CancelButtonText
	{
		get { return Get( () => CancelButtonText, Globals.Resources.GetStringResource( "Exit" ) ); }
		set { Set( () => CancelButtonText, value ); }
	}

	public Func<Task<bool>> OnConfirmCancelScan = null!,
	                        OnConfirmExit       = null!;

	public ICommand OnScanCancelButtonClick => Commands[ nameof( OnScanCancelButtonClick ) ];

	public async void Execute_OnScanCancelButtonClick()
	{
		try
		{
			Reference = "";

			if( TripDisplayEntries.Count > 0 )
			{
				if( await OnConfirmCancelScan() )
					Execute_OnCancelButtonClick();
			}
			else if( await OnConfirmExit() )
				MainPageViewModel.Instance.Show( MainPageViewModel.STATE.HOME );
		}
		catch
		{
		}
	}
#endregion

#region Pod and Sign
	public Action<bool> OnShowPodAndSignature = null!,
	                    OnShowSignature       = null!;

	public void CancelSignature()
	{
		OnShowSignature( false );
		Signature = null;
	}

	public void OkSignature( SignatureResult signature )
	{
		Signature = signature;
		OnShowSignature( false );
	}


	public ICommand OnFinalisePodAndSignatureButtonClick => Commands[ nameof( OnFinalisePodAndSignatureButtonClick ) ];

	public void Execute_OnFinalisePodAndSignatureButtonClick()
	{
		var Pod     = POD;
		var Entries = new List<TripDisplayEntry>( TripDisplayEntries );
		var Sig     = Signature!;

		// ReSharper disable once MethodHasAsyncOverload
		Tasks.RunVoid( () =>
		               {
			               try
			               {
				               foreach( var Entry in Entries )
				               {
					               Entry.Barcode = Entry.Reference; // Reference gets replaced with barcode during update
					               Entry.PopPod  = Pod;
				               }
				               TripUpdate.Update( Entries, Sig, Pod, Service.STATUS.VERIFIED );
			               }
			               catch( Exception e )
			               {
				               Debug.WriteLine( e );
			               }
		               } );

		Execute_OnCancelButtonClick();
	}


	public ICommand OnSignButtonClick => Commands[ nameof( OnSignButtonClick ) ];

	public void Execute_OnSignButtonClick()
	{
		OnShowSignature( true );
	}


	public ICommand OnCancelPodSign => Commands[ nameof( OnCancelPodSign ) ];

	public void Execute_OnCancelPodSign()
	{
		POD       = "";
		Signature = null;
		OnShowPodAndSignature( false );
	}

	public bool DoPodAndSign()
	{
		OnShowPodAndSignature( true );
		return false;
	}

	[DependsUpon250( nameof( POD ) )]
	[DependsUpon250( nameof( Signature ) )]
	public void WhenPodOrSignatureChanges()
	{
		PodAndSignAcceptEnable = Signature is not null && POD.IsNotNullOrWhiteSpace();
	}

	public bool PodAndSignAcceptEnable
	{
		get { return Get( () => PodAndSignAcceptEnable, false ); }
		set { Set( () => PodAndSignAcceptEnable, value ); }
	}

	public string POD
	{
		get { return Get( () => POD, "" ); }
		set { Set( () => POD, value ); }
	}


	public SignatureResult? Signature
	{
		get { return Get( () => Signature, (SignatureResult?)null ); }
		set { Set( () => Signature, value ); }
	}
#endregion
}