﻿namespace MetroReturns.Views.Warehouse;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class WarehouseClaims : ContentView
{
	public WarehouseClaims()
	{
		InitializeComponent();

		var M = (WarehouseClaimsViewModel)Root.BindingContext;

		void Show( View toView, View? fromView = null )
		{
			if( fromView is not null )
				fromView.IsVisible = false;

			toView.IsVisible = true;
			RaiseChild( toView );
			toView.FadeTo( 1, 500, Easing.CubicIn );
		}

		M.ShowScanner = show =>
		                {
			                if( show )
			                {
				                Show( Scanner );
				                M.BarcodeScannerEnabled = true;
				                BarcodeScanner.Focus();
			                }
			                else
				                Show( Root, Scanner );
		                };

		Task Error( string text )
		{
			return ErrorDialog.Execute( text );
		}

		M.OnFocusReturnId = () =>
		                    {
			                    Scanner.Focus();
		                    };

		M.OnBadReferenceId = async () =>
		                     {
			                     await Error( "ReferenceNumericOnly" );
		                     };

		M.OnNoShipmentsFound = async () =>
		                       {
			                       await Error( "NoShipmentsFound" );
		                       };

		M.OnAlreadyScanned = async reference =>
		                     {
			                     var Text = $"""
			                                 {Globals.Resources.GetStringResource( "Reference" )}: {reference}
			                                 {Globals.Resources.GetStringResource( "AlreadyScanned" )}
			                                 """;
			                     await Error( Text );
		                     };

		M.OnNotPickedUp = async reference =>
		                  {
			                  var Text = $"""
			                              {Globals.Resources.GetStringResource( "Reference" )}: {reference}
			                              {Globals.Resources.GetStringResource( "NotPickedUp" )}
			                              """;
			                  await Error( Text );
		                  };

		M.OnAlreadyProcessed = async reference =>
		                       {
			                       var Text = $"""
			                                   {Globals.Resources.GetStringResource( "Reference" )}: {reference}
			                                   {Globals.Resources.GetStringResource( "AlreadyProcessed" )}
			                                   """;

			                       await Error( Text );
		                       };

		M.OnConfirmCancelScan = () => ConfirmDialog.Execute( "CancelScan" );

		M.OnConfirmExit = () => ConfirmDialog.Execute( "ExitToMenu" );

		M.OnConfirmFinalise = () => ConfirmDialog.Execute( "FinaliseClaim" );

		M.OnShowPodAndSignature = show =>
		                          {
			                          if( show )
			                          {
				                          M.BarcodeScannerEnabled = false;
				                          Signature.Clear();
				                          Show( PodAndSign );

				                          Tasks.RunVoid( 100, () =>
				                                              {
					                                              Dispatcher.BeginInvokeOnMainThread( () =>
					                                                                                  {
						                                                                                  Pod.Focus();
					                                                                                  } );
				                                              } );
			                          }
			                          else
				                          Show( Root, PodAndSign );
		                          };

		M.OnShowSignature = show =>
		                    {
			                    if( show )
				                    Show( Signature, PodAndSign );
			                    else
				                    Show( PodAndSign, Signature );
		                    };

		Signature.OnCancel = () =>
		                     {
			                     M.CancelSignature();
		                     };

		Signature.OnOk = signature =>
		                 {
			                 M.OkSignature( signature );
		                 };

		M.ShowScanner( true );
	}
}