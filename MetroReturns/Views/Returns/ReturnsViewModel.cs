﻿// ReSharper disable InconsistentNaming

namespace MetroReturns.Views;

internal class ReturnsViewModel : ViewModelBase
{
	private const string ACCOUNT_ID = "rbandh_metro";

	public ObservableCollection<TripDisplayEntry> TripDisplayEntries
	{
		get { return Get( () => TripDisplayEntries, [] ); }
		set { Set( () => TripDisplayEntries, value ); }
	}


	public TripDisplayEntry? SelectedItem
	{
		get { return Get( () => SelectedItem, (TripDisplayEntry?)null ); }
		set { Set( () => SelectedItem, value ); }
	}


	public Action OnBadReturnId                 = null!,
	              OnBadReturnIdAlreadyProcessed = null!,
	              OnNoShipmentsFound            = null!,
	              OnNextReturnId                = null!,
	              OnFocusReturnId               = null!;

	public Action<bool> ShowScanner    = null!,
	                    ShowPodAndSign = null!,
	                    ShowSignature  = null!;

	[DependsUpon( nameof( SelectedItem ) )]
	public void WhenSelectedItemChanges()
	{
		var Item = SelectedItem;

		if( Item is not null )
		{
			Barcode          = "";
			ScanAcceptEnable = false;
			ShowScanner( true );
			BarcodeScannerEnabled = true;
		}
	}

	public async void ReturnIdCompleted()
	{
		var Rid = ReturnId.Trim();

		if( Rid.IsInteger() )
		{
			if( !ProcessedEntries.ContainsKey( Rid ) )
			{
				var Trps = await Service.Client.FindTripsByPalletAsync( ACCOUNT_ID, Rid );

				var Trips = ( from T in Trps
				              where T.status is (int)Service.STATUS.ACTIVE or (int)Service.STATUS.DISPATCHED
				                    && ( T.IsCWD() || T.IsReturn() )
				              select T ).ToList();

				if( Trips.Count > 0 )
				{
					ReturnIdVisible  = false;
					BackToAppVisible = false;

					var Entries = new ObservableCollection<TripDisplayEntry>( await Trips.Convert() );
					TripDisplayEntries = Entries;

					if( Entries.Count > 0 )
					{
						var E0 = Entries[ 0 ];
						CompanyName    = E0.PickupCompany;
						CompanyAddress = E0.PickupAddress;

						FinalisedEnabled         = false;
						BackToAppVisible         = false;
						BackToAppFinaliseVisible = false;
						CompanyVisible           = true;
						CancelEnabled            = true;
						return;
					}
				}
				ReturnId = "";
				OnNoShipmentsFound();
			}
			else
			{
				ReturnId = "";
				OnBadReturnIdAlreadyProcessed();
			}
		}
		else
			OnBadReturnId();
	}

#region Return Id
	public bool ReturnIdVisible
	{
		get { return Get( () => ReturnIdVisible, true ); }
		set { Set( () => ReturnIdVisible, value ); }
	}

	public bool ReturnIdNotVisible
	{
		get { return Get( () => ReturnIdNotVisible, false ); }
		set { Set( () => ReturnIdNotVisible, value ); }
	}

	public string ReturnId
	{
		get { return Get( () => ReturnId, "" ); }
		set { Set( () => ReturnId, value ); }
	}

	[DependsUpon( nameof( ReturnIdVisible ) )]
	public void WhenReturnIdVisibleChanges()
	{
		ReturnIdNotVisible = !ReturnIdVisible;
	}
#endregion

#region Cancel Button
	public enum CANCEL_ACTION
	{
		DONT_CANCEL,
		THIS_REQUEST_ID,
		ALL
	}

	public enum CANCEL_DELEGATE
	{
		SINGLE,
		BOTH,
		ALL
	}

	public delegate Task<CANCEL_ACTION> ConfirmCancelDelegate( CANCEL_DELEGATE cancelType );

	public ConfirmCancelDelegate OnConfirmCancel = null!;

	public ICommand OnCancelButtonClick => Commands[ nameof( OnCancelButtonClick ) ];

	public bool CanExecute_OnCancelButtonClick() => CancelEnabled;

	public bool CancelEnabled
	{
		get { return Get( () => CancelEnabled, false ); }
		set { Set( () => CancelEnabled, value ); }
	}

	public void ProcessNextId()
	{
		TripDisplayEntries    = [];
		CompanyVisible        = false;
		BarcodeScannerEnabled = false;
		NextReturnIdEnabled    = false;
		ReturnIdVisible       = true;
		CancelEnabled         = true;
		ShowScanner( false );
		ShowSignature( false );
		ReturnId = "";
		UserName = "";
		SetBackToAppVisible();
		OnFocusReturnId();
	}

	public async void Execute_OnCancelButtonClick()
	{
		var Has = HasProcessedEntries;

		CANCEL_DELEGATE Cancel;

		if( ReturnIdVisible )
			Cancel = Has ? CANCEL_DELEGATE.ALL : CANCEL_DELEGATE.SINGLE;
		else
			Cancel = Has ? CANCEL_DELEGATE.BOTH : CANCEL_DELEGATE.SINGLE;

		switch( await OnConfirmCancel( Cancel ) )
		{
		case CANCEL_ACTION.ALL:
			ProcessedEntries.Clear();
			goto case CANCEL_ACTION.THIS_REQUEST_ID;

		case CANCEL_ACTION.THIS_REQUEST_ID:
			ProcessNextId();
			break;
		}
	}
#endregion

#region Next Return Id
	private readonly Dictionary<string, ObservableCollection<TripDisplayEntry>> ProcessedEntries = [];

	private bool HasProcessedEntries => ProcessedEntries.Count > 0;

	public ICommand OnNextReturnIdClick => Commands[ nameof( OnNextReturnIdClick ) ];

	public bool CanExecute_OnNextReturnIdClick => NextReturnIdEnabled;

	public void Execute_OnNextReturnIdClick()
	{
		ProcessedEntries.Add( ReturnId.Trim(), TripDisplayEntries );
		ProcessNextId();
	}

	public bool NextReturnIdEnabled
	{
		get { return Get( () => NextReturnIdEnabled, false ); }
		set { Set( () => NextReturnIdEnabled, value ); }
	}
#endregion

#region Finalise
	public ICommand OnCompleteReturnButtonClick => Commands[ nameof( OnCompleteReturnButtonClick ) ];

	public bool CanExecute_OnCompleteReturnButtonClick() => PodAndSignAcceptEnable;

	public void Execute_OnCompleteReturnButtonClick()
	{
		var Rid = ReturnId.Trim();

		if( !ProcessedEntries.ContainsKey( Rid ) )
			ProcessedEntries.Add( Rid, TripDisplayEntries );

		List<TripDisplayEntry> Entries = [];

		foreach( var ProcessedEntry in ProcessedEntries )
			Entries.AddRange( ProcessedEntry.Value );

		var UName = UserName;

		foreach( var Entry in Entries )
			Entry.PopPod = UName;

		TripUpdate.Update( Entries, Signature!, Login.UserName, Service.STATUS.PICKED_UP );
		ShowPodAndSign( false );
		ProcessedEntries.Clear();
		TripDisplayEntries.Clear();
		ProcessNextId();
	}

	public bool FinalisedEnabled
	{
		get { return Get( () => FinalisedEnabled, false ); }
		set { Set( () => FinalisedEnabled, value ); }
	}


	public string UserName
	{
		get { return Get( () => UserName, "" ); }
		set { Set( () => UserName, value ); }
	}


	public SignatureResult? Signature
	{
		get { return Get( () => Signature, (SignatureResult?)null ); }
		set { Set( () => Signature, value ); }
	}

	[DependsUpon( nameof( Signature ) )]
	[DependsUpon( nameof( UserName ) )]
	public void EnablePodSignFinalise()
	{
		PodAndSignAcceptEnable = ( UserName.Trim() != "" ) && Signature is not null && ( Signature.Points.Length > 3 );
	}

	public bool PodAndSignAcceptEnable
	{
		get { return Get( () => PodAndSignAcceptEnable, false ); }
		set { Set( () => PodAndSignAcceptEnable, value ); }
	}

	public ICommand OnSignButtonClick => Commands[ nameof( OnSignButtonClick ) ];

	public void Execute_OnSignButtonClick()
	{
		ShowSignature( true );
	}

	public ICommand OnFinalisedButtonClick => Commands[ nameof( OnFinalisedButtonClick ) ];

	public void Execute_OnFinalisedButtonClick()
	{
		BarcodeScannerEnabled = false;
		FinalisedEnabled      = false;
		CancelEnabled         = false;
		NextReturnIdEnabled    = false;
		ShowPodAndSign( true );
	}


	public ICommand OnCancelPodSign => Commands[ nameof( OnCancelPodSign ) ];

	public void Execute_OnCancelPodSign()
	{
		FinalisedEnabled = true;
		CancelEnabled    = true;
		ShowPodAndSign( false );
	}
#endregion

#region Scanning
	public bool BarcodeScannerEnabled
	{
		get { return Get( () => BarcodeScannerEnabled, false ); }
		set { Set( () => BarcodeScannerEnabled, value ); }
	}

	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	[DependsUpon( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		ScanAcceptEnable = Barcode.Trim() != "";
	}

	public bool ScanAcceptEnable
	{
		get { return Get( () => ScanAcceptEnable, false ); }
		set { Set( () => ScanAcceptEnable, value ); }
	}


	public string CompanyName
	{
		get { return Get( () => CompanyName, "" ); }
		set { Set( () => CompanyName, value ); }
	}


	public string CompanyAddress
	{
		get { return Get( () => CompanyAddress, "" ); }
		set { Set( () => CompanyAddress, value ); }
	}


	public bool CompanyVisible
	{
		get { return Get( () => CompanyVisible, false ); }
		set { Set( () => CompanyVisible, value ); }
	}


	public ICommand OnScanCancelButtonClick => Commands[ nameof( OnScanCancelButtonClick ) ];

	public void Execute_OnScanCancelButtonClick()
	{
		Barcode      = "";
		SelectedItem = null;
		ShowScanner( false );
	}

	public ICommand OnScanAcceptButtonClick => Commands[ nameof( OnScanAcceptButtonClick ) ];

	public bool CanExecute_OnScanAcceptButtonClick() => ScanAcceptEnable;

	public void Execute_OnScanAcceptButtonClick()
	{
		ShowScanner( false );

		var BCode = Barcode.Trim();

		if( BCode != "" )
		{
			Barcode = "";

			var Sel = SelectedItem;

			if( Sel is not null )
			{
				SelectedItem = null;

				bool HasCode( IEnumerable<TripDisplayEntry> e )
				{
					return e.Any( Entry => ( Entry != Sel ) && ( Entry.Barcode == BCode ) );
				}

				var Entries = TripDisplayEntries;

				if( !( HasCode( Entries ) || ProcessedEntries.Any( pair => HasCode( pair.Value ) ) ) )
				{
					Sel.Barcode   = BCode;
					Sel.IsScanned = true;

					var Ndx = Entries.IndexOf( Sel );

					if( Ndx >= 0 )
					{
						Entries.RemoveAt( Ndx );
						Entries.Add( Sel );
					}

					var Enable = Entries.All( entry => entry.IsScanned );

					FinalisedEnabled   = Enable;
					NextReturnIdEnabled = Enable;
				}
				else
					ErrorDialog.Execute( "Barcode Already Scanned" );
			}
		}
	}
#endregion

#region Exit Application
	private void SetBackToAppVisible()
	{
		SetBackToAppVisible( !HasProcessedEntries );
	}

	private void SetBackToAppVisible( bool backToAppVisible )
	{
		BackToAppVisible         = backToAppVisible;
		BackToAppFinaliseVisible = !backToAppVisible;
	}

	public bool BackToAppVisible
	{
		get { return Get( () => BackToAppVisible, true ); }
		set { Set( () => BackToAppVisible, value ); }
	}


	public bool BackToAppFinaliseVisible
	{
		get { return Get( () => BackToAppFinaliseVisible, false ); }
		set { Set( () => BackToAppFinaliseVisible, value ); }
	}

	public ICommand OnBackToMenuButtonClick => Commands[ nameof( OnBackToMenuButtonClick ) ];

	public void Execute_OnBackToMenuButtonClick()
	{
		MainPageViewModel.Instance.Show( MainPageViewModel.STATE.HOME );
	}
#endregion
}