﻿using ViewModelsBase;

namespace MetroReturns.Views;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Returns : ContentView
{
	public Returns()
	{
		InitializeComponent();

		var M = (ReturnsViewModel)Root.BindingContext;

		void OnFocusReturnId()
		{
			( (AViewModelBase)M ).Dispatcher?.Invoke( () =>
			                                          {
				                                          ReturnId.Focus();
			                                          } );
		}

		M.OnFocusReturnId = OnFocusReturnId;

		Signature.OnCancel = () =>
		                     {
			                     Signature.Clear();
			                     M.ShowSignature( false );
		                     };

		Signature.OnOk = signature =>
		                 {
			                 M.Signature = signature;
			                 M.ShowSignature( false );
		                 };

		M.OnNextReturnId = () =>
		                   {
			                   PodAndSign.FadeTo( 0, 500, Easing.CubicOut );
			                   RaiseChild( Root );
			                   PodAndSign.IsVisible = false;
		                   };

		M.ShowPodAndSign = show =>
		                   {
			                   if( show )
			                   {
				                   Scanner.IsVisible = false;
				                   Signature.Clear();
				                   PodAndSign.IsVisible = true;
				                   RaiseChild( PodAndSign );
				                   PodAndSign.FadeTo( 1, 500, Easing.CubicIn );

				                   Tasks.RunVoid( 100, () =>
				                                       {
					                                       Dispatcher.BeginInvokeOnMainThread( () =>
					                                                                           {
						                                                                           UserName.Focus();
					                                                                           } );
				                                       } );
			                   }
			                   else
				                   M.OnNextReturnId();
		                   };

		M.ShowSignature = show =>
		                  {
			                  if( show )
			                  {
				                  Signature.IsVisible = true;
				                  RaiseChild( Signature );
				                  Orientation.LayoutDirection = Orientation.ORIENTATION.LANDSCAPE;
				                  Signature.FadeTo( 1, 500, Easing.CubicIn );
			                  }
			                  else
			                  {
				                  Signature.FadeTo( 0, 500, Easing.CubicOut );
				                  RaiseChild( Root );
				                  Orientation.LayoutDirection = Orientation.ORIENTATION.PORTRAIT;
				                  Signature.IsVisible         = false;
			                  }
		                  };

		M.ShowScanner = show =>
		                {
			                if( show )
			                {
				                Scanner.IsVisible = true;
				                RaiseChild( Scanner );
				                Scanner.FadeTo( 1, 500, Easing.CubicIn );
				                BarcodeScanner.Focus();
			                }
			                else
			                {
				                Scanner.FadeTo( 0, 500, Easing.CubicOut );
				                RaiseChild( Root );
				                Scanner.IsVisible = false;
			                }
		                };

		ReturnId.Completed += ( _, _ ) =>
		                      {
			                      M.ReturnIdCompleted();
		                      };

		async Task Error( string text )
		{
			await ErrorDialog.Execute( text );
			ReturnId.Focus();
		}

		M.OnBadReturnId = async () =>
		                  {
			                  await Error( "ErrorId7" );
		                  };

		M.OnNoShipmentsFound = async () =>
		                       {
			                       await Error( "NoShipmentsFound" );
		                       };

		M.OnBadReturnIdAlreadyProcessed = async () =>
		                                  {
			                                  await Error( "ReturnIdAlreadyProcessed" );
		                                  };

		M.OnConfirmCancel = async cancel =>
		                    {
			                    try
			                    {
				                    switch( cancel )
				                    {
				                    case ReturnsViewModel.CANCEL_DELEGATE.BOTH:
					                    if( await ConfirmDialog.Execute( "CancelJustThisReturn" ) )
						                    return ReturnsViewModel.CANCEL_ACTION.THIS_REQUEST_ID;
					                    goto case ReturnsViewModel.CANCEL_DELEGATE.ALL;

				                    case ReturnsViewModel.CANCEL_DELEGATE.ALL:
					                    if( await ConfirmDialog.Execute( "CancelAllReturns" ) )
						                    return ReturnsViewModel.CANCEL_ACTION.ALL;
					                    break;

				                    case ReturnsViewModel.CANCEL_DELEGATE.SINGLE:
					                    if( await ConfirmDialog.Execute( "CancelThisReturn" ) )
						                    return ReturnsViewModel.CANCEL_ACTION.ALL;
					                    break;
				                    }
			                    }
			                    catch
			                    {
			                    }
			                    return ReturnsViewModel.CANCEL_ACTION.DONT_CANCEL;
		                    };

		M.ProcessNextId();
	}
}