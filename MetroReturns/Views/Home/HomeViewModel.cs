﻿using Application = Globals.Application;

namespace MetroReturns.Views;

public class HomeViewModel : ViewModelBase
{
	public Func<Task<bool>> OnConfirmExitApplication = null!;

#region Actions
#region Warehouse
	public ICommand OnCwdReturns => Commands[ nameof( OnCwdReturns ) ];

	public void Execute_OnCwdReturns()
	{
		MainPageViewModel.Instance.Show( MainPageViewModel.STATE.RETURNS );
	}
#endregion

#region Driver Claims
	public ICommand OnDriverClaims => Commands[ nameof( OnDriverClaims ) ];

	public void Execute_OnDriverClaims()
	{
		MainPageViewModel.Instance.Show( MainPageViewModel.STATE.DRIVER_CLAIMS );
	}
#endregion

#region Warehouse Claims
	public ICommand OnWarehouseClaims => Commands[ nameof( OnWarehouseClaims ) ];

	public void Execute_OnWarehouseClaims()
	{
		MainPageViewModel.Instance.Show( MainPageViewModel.STATE.WAREHOUSE_CLAIMS );
	}
#endregion

#region Driver Verification
	public ICommand OnDriverVerification => Commands[ nameof( OnDriverVerification ) ];

	public void Execute_OnDriverVerification()
	{
		MainPageViewModel.Instance.Show( MainPageViewModel.STATE.DRIVER_VERIFICATION );
	}
#endregion

#region Dsd Deliveries
	public ICommand OnDsdDeliveries => Commands[ nameof( OnDsdDeliveries ) ];

	public void Execute_OnDsdDeliveries()
	{
		MainPageViewModel.Instance.Show( MainPageViewModel.STATE.DSD_DELIVERIES );
	}
#endregion

	public ICommand OnExitApplication => Commands[ nameof( OnExitApplication ) ];

	public async void Execute_OnExitApplication()
	{
		try
		{
			if( await OnConfirmExitApplication() )
				Application.ExitApplication();
		}
		catch
		{
		}
	}
#endregion
}