﻿namespace MetroReturns.Views;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Home : ContentView
{
	public Home()
	{
		InitializeComponent();
		var M = (HomeViewModel)Root.BindingContext;

		async Task<bool> Confirm( string text )
		{
			return await ConfirmDialog.Execute( text );
		}

		M.OnConfirmExitApplication = async () => await Confirm( "ExitApplication" );
	}
}