﻿namespace MetroReturns.Views.DriverVerification;

internal class VerificationViewModel : TripsViewModelBase
{
	protected override void OnInitialised()
	{
		base.OnInitialised();
		StatusFilter = [Service.STATUS.DISPATCHED];
		WhenWaitingForTripsChanges();
	}

#region Status
	public new Service.STATUS[] StatusFilter
	{
		get { return Get( () => StatusFilter, [] ); }
		set { Set( () => StatusFilter, value ); }
	}

	[DependsUpon( nameof( StatusFilter ) )]
	public void WhenStatusFilterChanges()
	{
		base.StatusFilter = StatusFilter;
	}
#endregion

#region Accept
	public Func<Task<bool>>? OnConfirmAccept;

	public bool EnableAccept
	{
		get { return Get( () => EnableAccept, false ); }
		set { Set( () => EnableAccept, value ); }
	}

	public ICommand OnAccept => Commands[ nameof( OnAccept ) ];

	public bool CanExecute_OnAccept() => EnableAccept;

	public async void Execute_OnAccept()
	{
		try
		{
			if( OnConfirmAccept is not null && await OnConfirmAccept() )
			{
				var TripIds = new List<string>();

				foreach( var Trip in FilterTrips() )
				{
					Trip.Status = Service.STATUS.PICKED_UP;
					TripIds.Add( Trip.TripId );
				}

				TripUpdate.PickupTrips( TripIds, DateTimeOffset.Now );

				MainPage.DsdDeliveries();
			}
		}
		catch
		{
		}
	}
#endregion

#region Filters
	private static bool FilterTrips( TripDisplayEntry trip ) => trip.IsDsd;

	protected override bool OnFilterTrip( TripDisplayEntry trip ) => FilterTrips( trip );

	public Func<TripDisplayEntry, bool> DisableServiceLevelFilter
	{
		get { return Get( () => DisableServiceLevelFilter, FilterTrips ); }
		set { Set( () => DisableServiceLevelFilter, value ); }
	}
#endregion

#region Barcode
	public string Barcode
	{
		get { return Get( () => Barcode, "" ); }
		set { Set( () => Barcode, value ); }
	}

	public Action? OnNotForThisRouteAction;

	[DependsUpon250( nameof( Barcode ) )]
	public void WhenBarcodeChanges()
	{
		var Trps = FilterTrips();

		try
		{
			var BCode = Barcode.Trim();

			foreach( var Trip in Trps )
			{
				if( Trip.TripId == BCode )
				{
					if( Trip.IsVisible )
						Trip.IsVisible = false;
					else
						Sounds.PlayBadScanSound();
					return;
				}
			}
			OnNotForThisRouteAction?.Invoke();
		}
		finally
		{
			EnableAccept = Trps.All( trip => !trip.IsVisible );
		}
	}

	public bool Scanning
	{
		get { return Get( () => Scanning, false ); }
		set { Set( () => Scanning, value ); }
	}
#endregion

#region Waiting For Trips
	public Action<bool>? OnWaitingForTrips;

	public bool WaitingForTrips
	{
		get { return Get( () => WaitingForTrips, false ); }
		set { Set( () => WaitingForTrips, value ); }
	}

	[DependsUpon( nameof( WaitingForTrips ) )]
	public void WhenWaitingForTripsChanges()
	{
		var Waiting = WaitingForTrips;
		OnWaitingForTrips?.Invoke( Waiting );

		if( !Waiting )
			Scanning = true;
	}

#region Cancel
	public ICommand OnCancel => Commands[ nameof( OnCancel ) ];

	public void Execute_OnCancel()
	{
		MainPage.Home();
	}

	public ICommand OnCancelScan => Commands[ nameof( OnCancelScan ) ];

	public bool CanExecute_OnCancelScan() => true;

	public Action? OnCancelScanAction;

	public void Execute_OnCancelScan()
	{
		OnCancelScanAction?.Invoke();
	}
#endregion
#endregion
}