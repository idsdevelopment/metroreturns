﻿namespace MetroReturns.Views.DriverVerification;

[XamlCompilation( XamlCompilationOptions.Compile )]
public partial class Verification : ContentView
{
	public Verification()
	{
		InitializeComponent();
		var Model = (VerificationViewModel)Root.BindingContext;

		Model.OnWaitingForTrips = waiting =>
		                          {
			                          ViewModelBase.Dispatcher( () =>
			                                                    {
				                                                    Root.RaiseChild( waiting ? WaitingForTrips : DriverVerification );
			                                                    } );
		                          };

		Model.OnCancelScanAction = async () =>
		                           {
			                           if( await ConfirmDialog.Execute( "CancelScan" ) )
				                           MainPage.Home();
		                           };

		Model.OnNotForThisRouteAction = () =>
		                                {
			                                ErrorDialog.Execute( "ToteNotForRoute" );
		                                };

		Model.OnConfirmAccept = async () => await ConfirmDialog.Execute( "ClaimTotess" );

		Model.OnWaitingForTrips( true );
	}
}