﻿namespace IdsRemoteService;

public partial class Service : IDSWSService
{
	public bool Ping()
	{
		try
		{
			var Ok = Client.ping( new ping() )?.@return;

			return Ok is not null && ( Ok.Compare( "OK", StringComparison.OrdinalIgnoreCase ) == 0 );
		}
		catch
		{
		}
		return false;
	}

	public async Task<bool> PingAsync()
	{
		return await Task.Run( Ping );
	}
}