﻿namespace IdsRemoteService;

public partial class Service : IDSWSService
{
	private readonly MemoryAddressCache AddressCache;

	private class MemoryAddressCache : MemoryCache<string, remoteAddress>
	{
		private readonly Service Owner;

		internal MemoryAddressCache( Service owner )
		{
			Owner = owner;
		}

		internal async Task<remoteAddress?> GetRemoteAddressAsync( string addressId )
		{
			var (Ok, Address) = TryGetValue( addressId );

			if( !Ok )
			{
				var Addr = await Owner.FindAddressByIdAsync( addressId );

				if( Addr is not null )
				{
					AddSliding( addressId, Addr, TimeSpan.FromDays( 1 ) );
					return Addr;
				}
			}
			return Address;
		}
	}

	public async Task<(bool Ok, remoteAddress Address)> GetRemoteAddressAsync( string addressId )
	{
		var Address = await AddressCache.GetRemoteAddressAsync( addressId );
		return ( Address is not null, Address! );
	}


	public remoteAddress? FindAddressById( string addressId )
	{
		try
		{
			return Client.findAddress( new findAddress
			                           {
				                           authToken = AuthToken,
				                           addressId = addressId
			                           } )?.@return;
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return null;
	}

	public async Task<remoteAddress?> FindAddressByIdAsync( string addressId )
	{
		return await Task.Run( () => FindAddressById( addressId ) );
	}
}