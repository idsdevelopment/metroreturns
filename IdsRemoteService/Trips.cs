﻿namespace IdsRemoteService;

public partial class Service : IDSWSService
{
	public void UpdateRbhAssets( string accountId, string tripId, IEnumerable<string> assetBarcodes )
	{
		updateAssetTrips( new updateAssetTrips
		                  {
			                  authToken     = AuthToken,
			                  accountId     = accountId,
			                  tripId        = tripId,
			                  assetBarcodes = assetBarcodes.ToArray()
		                  } );
	}

	public remoteTrip[] FindTripsForDriver( string driverCode )
	{
		try
		{
			return findTripsForDriver( new findTripsForDriver
			                           {
				                           authToken = AuthToken,
				                           driverId  = driverCode
			                           } ) ?? [];
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return [];
	}

	public bool UpdateTripDetailedQuick( remoteTripDetailed t )
	{
		try
		{
			updateTripDetailedQuick( new updateTripDetailedQuick
			                         {
				                         authToken = AuthToken,
				                         _tripVal  = t
			                         } );
			return true;
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return false;
	}

	public void PickupTrips( List<string> tripIds, DateTimeOffset updateTime )
	{
		var Count = tripIds.Count;

		if( Count > 0 )
		{
			try
			{
				var UpTime = updateTime.LocalTimeToServerTime();

				var Dates = new DateTime[ Count ];

				for( var I = 0; I < Count; I++ )
					Dates[ I ] = UpTime;

				var TripIds = tripIds.ToArray();

				setTripsToPickedUp( new setTripsToPickedUp
				                    {
					                    authToken = AuthToken,
					                    puDates   = Dates,
					                    trips     = TripIds
				                    } );
			}
			catch( Exception Exception )
			{
				Debug.WriteLine( Exception );
			}
		}
	}

	public remoteTrip[] FindTripsByPallet( string accountId, string palletString )
	{
		try
		{
			return findTripsByPalletsField( new findTripsByPalletsField
			                                {
				                                authToken    = AuthToken,
				                                accountId    = accountId,
				                                palletsField = palletString
			                                } ) ?? [];
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return [];
	}

	public remoteTrip[] FindTripsByReference( string reference )
	{
		try
		{
			var Temp = findTripByClientReferenceId( new findTripByClientReferenceId
			                                        {
				                                        authToken       = AuthToken,
				                                        clientReference = reference
			                                        } );

			if( Temp?.@return is { } Trip )
				return [Trip];
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return [];
	}


	public Task<remoteTrip[]> FindTripsByPalletAsync( string accountId, string palletString )
	{
		return Task.Run( () => FindTripsByPallet( accountId, palletString ) );
	}

	public Task<remoteTrip[]> FindTripsReferenceAsync( string reference )
	{
		return Task.Run( () => findTripsByClientReferenceIdExact( new findTripsByClientReferenceIdExact
		                                                          {
			                                                          authToken       = AuthToken,
			                                                          clientReference = reference
		                                                          } ) );
	}

	public remoteAddress? FindAddress( string addressId )
	{
		try
		{
			var Address = findAddress( new findAddress
			                           {
				                           authToken = AuthToken,
				                           addressId = addressId
			                           } );

			if( Address is not null )
				return Address.@return;
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return null;
	}

	public remoteTrip? FindTrip( string tripId )
	{
		try
		{
			var Trip = findTrip( new findTrip
			                     {
				                     authToken = AuthToken,
				                     tripId    = tripId
			                     } );

			if( Trip is not null )
				return Trip.@return;
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return null;
	}


	public remoteTrip? FindByTripIdOrReference( string id )
	{
		try
		{
			remoteTrip? ReferenceTrip = null,
			            Trip          = null;

			Task.WaitAll( Task.Run( () =>
			                        {
				                        var T = findTrip( new findTrip
				                                          {
					                                          authToken = AuthToken,
					                                          tripId    = id
				                                          } );

				                        if( T is not null )
					                        Trip = T.@return;
			                        } ),
			              Task.Run( () =>
			                        {
				                        var R = findTripByClientReferenceIdExact( new findTripByClientReferenceIdExact
				                                                                  {
					                                                                  authToken       = AuthToken,
					                                                                  clientReference = id
				                                                                  } );

				                        if( R is not null )
					                        ReferenceTrip = R.@return;
			                        } ) );

			return ReferenceTrip ?? Trip;
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return null;
	}

	public bool UpdateTrip( remoteTrip trip )
	{
		try
		{
			updateTripQuickBroadcast( new updateTripQuickBroadcast
			                          {
				                          authToken        = AuthToken,
				                          _tripVal         = trip,
				                          updateDriverZone = false
			                          } );
			return true;
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception.Message );
		}
		return false;
	}
}