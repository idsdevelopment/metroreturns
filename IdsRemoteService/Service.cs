﻿namespace IdsRemoteService;

[WebServiceBinding( Name = "IDSWSBinding", Namespace = "http://remote.server.id.com/" )]
public partial class Service : IDSWSService
{
	public const string
		JBTEST    = "http://jbtest.internetdispatcher.org:8080/ids-beans/IDSWS",
		JBTEST2   = "http://jbtest2.internetdispatcher.org:8080/ids-beans/IDSWS", // 198.50.187.106:8080
		TEST      = "http://test.internetdispatcher.org:8080/ids-beans/IDSWS",
		APP       = "http://app.internetdispatcher.org:8080/ids-beans/IDSWS",
		USERS     = "http://users.internetdispatcher.org:8080/ids-beans/IDSWS",
		REPORTS   = "http://reports.internetdispatcher.org:8080/ids-beans/IDSWS",
		DRIVERS   = "http://drivers.internetdispatcher.org:8080/ids-beans/IDSWS",
		IDS       = "http://ids.internetdispatcher.org:8080/ids-beans/IDSWS",
		OZ        = "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS",
		OZ_MIRROR = "http://ozmirror.internetdispatcher.org:8080/ids-beans/IDSWS",
		CUSTOM    = "http://custom.internetdispatcher.org:8080/ids-beans/IDSWS",

		//
		PEEK_JBTEST2   = "http://jbtest2.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_TEST      = "http://test.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_APP       = "http://app.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_USERS     = "http://users.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_REPORTS   = "http://reports.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_DRIVERS   = "http://drivers.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_IDS       = "http://ids.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_OZ        = "http://oz.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_OZ_MIRROR = "http://ozmirror.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS",
		PEEK_CUSTOM    = "http://custom.internetdispatcher.org:8080/peek-peekbeans/PeekManagerWS";

	public const int DEFAULT_TIMEOUT_IN_SECONDS = 60;

	private const string

	#if USE_JBTEST
		ENDPOINT_ADDRESS = JBTEST2;
	#else
		ENDPOINT_ADDRESS = DRIVERS;
#endif

	public enum STATUS
	{
		UNSET,
		NEW,
		ACTIVE,
		DISPATCHED,
		PICKED_UP,
		DELIVERED,
		VERIFIED,
		POSTED,
		DELETED,
		SCHEDULED,
		UNDELIVERED     = -3,
		CREATE_NEW_TRIP = -4
	}


	private Service()
	{
		Timeout      = DEFAULT_TIMEOUT_IN_SECONDS * 1000;
		Url          = ENDPOINT_ADDRESS;
		AddressCache = new MemoryAddressCache( this );
	}

	private static string AuthToken = "";
	public const   string METRO     = "Metro";

	public ( bool Ok, string UserName, bool Required ) Login( string userName, string password )
	{
		try
		{
			AuthToken = $"{METRO}-{userName}-{password}";

			var Result = findUserForLogin( new findUserForLogin
			                               {
				                               authToken    = AuthToken,
				                               accountId    = METRO,
				                               userIdToFind = userName,
				                               userPass     = password
			                               }
			                             )?.@return;
			bool Required;
			var  Ok = Result is { isEnabled: true };

			if( Ok )
			{
				Required = string.Compare( "REQUIRED", Result!.fax.Trim(), StringComparison.OrdinalIgnoreCase ) == 0;
				userName = Result.userId;
			}
			else
				Required = false;

			AuthToken = $"{METRO}-{userName}-{password}"; // Use user Id from Ids1

			return ( Ok, userName, Required );
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return ( false, userName, false );
	}

	public async Task<(bool Ok, string UserName, bool Required)> LoginAsync( string userName, string password )
	{
		return await Task.Run( () => Login( userName, password ) );
	}

	private static Service? _Client;
	public static  Service  Client => _Client ??= new Service();
}