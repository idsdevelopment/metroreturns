﻿using System;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using MetroReturns.Droid.Globals;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Xamarin.Essentials.Permissions;
using Keyboard = Globals.Keyboard;
using Platform = Xamarin.Essentials.Platform;
using Uri = Android.Net.Uri;

namespace MetroReturns.Droid;

[Activity( Label = "RB&H / Metro Courier Returns",
           Icon = "@mipmap/icon",
           Theme = "@style/SplashScreen",
           MainLauncher = true,
           AlwaysRetainTaskState = true,
           LaunchMode = LaunchMode.SingleTask,
           ClearTaskOnLaunch = true,
           ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden,
           ScreenOrientation = ScreenOrientation.Portrait )]
public partial class MainActivity : FormsAppCompatActivity
{
	public static MainActivity Instance { get; private set; } = null!;

	private bool Paused;

	public class InstallPackages : BasePlatformPermission
	{
		public override (string androidPermission, bool isRuntime)[] RequiredPermissions => new (string, bool)[]
		                                                                                    {
			                                                                                    ( Manifest.Permission.RequestInstallPackages, true )
		                                                                                    };
	}

	protected override void OnPause()
	{
		Paused = true;
		base.OnPause();
	}

	protected override void OnResume()
	{
		Paused = false;
		base.OnResume();
	}

	protected override async void OnCreate( Bundle? savedInstanceState )
	{
		Instance = this;

		base.OnCreate( savedInstanceState );

		Platform.Init( this, savedInstanceState );
		Forms.Init( this, savedInstanceState );

		Sounds.Init();
		Orientation.Init();

		Keyboard.ShowHide = show =>
		                    {
			                    Globals.Keyboard.Show = show;
		                    };

		LoadApplication( new App() );

		async Task PermissionCheck<T>( string text, Func<Task>? granted = null ) where T : BasePermission, new()
		{
			var Status = await CheckStatusAsync<T>();

			if( Status != PermissionStatus.Granted )
			{
				Status = await RequestAsync<T>();

				if( Status != PermissionStatus.Granted )
					ShowErrorDialogue( text ); // Does not return
			}

			if( granted is not null )
				await granted();
		}

		var MajorVersion = AndroidGlobals.Android.MajorVersion;

		await PermissionCheck<Camera>( "Camera",
		                               async () =>
		                               {
			                               const string INSTALL = "Install Packages";

			                               switch( MajorVersion )
			                               {
			                               case 7:
				                               await PermissionCheck<InstallPackages>( INSTALL );
				                               break;

			                               case >= 8:
				                               {
					                               if( PackageManager is not null && !PackageManager.CanRequestPackageInstalls() )
					                               {
						                               var SettingsIntent = new Intent( Settings.ActionManageUnknownAppSources );

						                               SettingsIntent.SetData( Uri.Parse( "package:" + PackageName ) );
						                               StartActivity( SettingsIntent );

						                               var Ok = await Task.Run( async () =>
						                                                        {
							                                                        while( !Paused )
								                                                        await Task.Delay( 1000 );

							                                                        while( Paused ) // Wait for Settings Dialog To Close
								                                                        await Task.Delay( 1000 );

							                                                        return PackageManager.CanRequestPackageInstalls();
						                                                        } );

						                               if( !Ok )
							                               ShowErrorDialogue( INSTALL );
					                               }
					                               break;
				                               }
			                               }
		                               }
		                             );
	}

	public override void OnRequestPermissionsResult( int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults )
	{
		Platform.OnRequestPermissionsResult( requestCode, permissions, grantResults );

		base.OnRequestPermissionsResult( requestCode, permissions, grantResults );
	}

	private void ShowErrorDialogue( string text )
	{
		var Alert = new AlertDialog.Builder( this );
		Alert.SetTitle( "Cannot continue" );
		Alert.SetMessage( $"Unable to continue without {text} permission." );

		Alert.SetPositiveButton( "Ok", ( _, _ ) =>
		                               {
			                               Finish();
			                               global::Globals.Application.ExitApplication();
		                               } );

		var Dialog = Alert.Create();
		Dialog?.Show();
	}
}