﻿using System.Threading.Tasks;
using Android.Media;
using System.Threading;

namespace MetroReturns.Droid.Globals
{
	internal static class Sounds
	{
		public static async void PlaySound( int sound, bool killSound )
		{
			if( !killSound )
			{
				await Task.Run( () =>
				                {
					                var Player = MediaPlayer.Create( MainActivity.Instance, sound );

					                if( Player is not null )
					                {
						                Player.Start();

						                do
							                Thread.Sleep( 50 );
						                while( Player.IsPlaying );

						                MainActivity.Instance?.RunOnUiThread( () =>
						                                                      {
							                                                      Player.Release();
							                                                      Player = null;
						                                                      } );
					                }
				                } );
			}
		}

		public static void Init()
		{
			global::Globals.Sounds.PlayBadScanSound = () => { PlaySound( Resource.Raw.BadScan, false ); };
			global::Globals.Sounds.PlayAlert1Sound  = () => { PlaySound( Resource.Raw.Alert1, false ); };
			global::Globals.Sounds.PlayWarningSound = () => { PlaySound( Resource.Raw.Warning, false ); };
		}
	}
}