﻿using Android.Content.PM;

namespace MetroReturns.Droid;

public class Orientation
{
	internal static void Init()
	{
		global::Globals.Orientation.ChangeOrientation = orientation =>
		                                                {
			                                                MainActivity.Instance.RunOnUiThread( () =>
			                                                                                     {
				                                                                                     MainActivity.Instance.RequestedOrientation = orientation switch
				                                                                                                                                  {
					                                                                                                                                  global::Globals.Orientation.ORIENTATION.LANDSCAPE => ScreenOrientation.Landscape,
					                                                                                                                                  global::Globals.Orientation.ORIENTATION.PORTRAIT  => ScreenOrientation.Portrait,
					                                                                                                                                  _                                                 => ScreenOrientation.Unspecified
				                                                                                                                                  };
			                                                                                     } );
		                                                };
	}
}