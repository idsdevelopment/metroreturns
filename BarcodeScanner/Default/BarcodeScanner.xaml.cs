﻿#nullable enable

using System;
using System.Timers;
using BarcodeInterface;
using Globals;
using Utils;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamlViewModel.Xamarin;
using ZXing.Mobile;
using Keyboard = Globals.Keyboard;

namespace DroidAz.Controls.Default
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class BarcodeScanner : ContentView, IBarcodeScanner
	{
	#region ManualBarcode
		public bool ManualBarcodeInput
		{
			get => Model.ManualBarcodeInput;
			set => Model.ManualBarcodeInput = value;
		}
	#endregion

		private static MobileBarcodeScanner? _Scanner;
		private static MobileBarcodeScanner  Scanner => _Scanner ??= new MobileBarcodeScanner();

		private readonly BarcodeScannerViewModel Model;

		public BarcodeScanner()
		{
			InitializeComponent();

			var M = (BarcodeScannerViewModel)Root.BindingContext;
			Model            = M;
			M.OwnerInterface = this;

			M.OnBarcode = barcode =>
			              {
				              Keyboard.Show = false;
				              OnBarcode?.Invoke( barcode );
			              };

			M.ExecuteZXingScanner = async () =>
			                        {
				                        var Tm = new Timer( 2_000 )
				                                 {
					                                 AutoReset = true
				                                 };

				                        Tm.Elapsed += ( _, _ ) => { ViewModelBase.Dispatcher( () => { Scanner.AutoFocus(); } ); };

				                        try
				                        {
					                        Tm.Start();
					                        return ( await Scanner.Scan( new MobileBarcodeScanningOptions
					                                                     {
																			 AutoRotate = true
					                                                     } ) )?.Text ?? "";
				                        }
				                        catch
				                        {
					                        return "";
				                        }
				                        finally
				                        {
					                        Tm.Dispose();
				                        }
			                        };

			M.ManualBarcodeInput = Preferences.AllowManualBarcode;

			BarcodeEntry.Unfocused += ( _, _ ) => { FocusDelayed(); };

			BarcodeEntry.Focused += ( _, _ ) => { Keyboard.Show = false; };
		}

	#region Focus
		private void FocusEntry()
		{
			if( IsEnabled && !BarcodeEntry.IsFocused )
			{
				BarcodeEntry.Focus();
				Keyboard.Show = false;
			}
		}

		private void FocusDelayed( int delay = 500 )
		{
			Tasks.RunVoid( delay, () => { ViewModelBase.Dispatcher( FocusEntry ); } );
		}

		bool IBarcodeScanner.Focus()
		{
			FocusEntry();
			return true;
		}
	#endregion

	#region Enabled
		private static void IsEnabledPropertyChanged( BindableObject bindable, object oldValue, object newValue )
		{
			if( bindable is BarcodeScanner Bs && newValue is bool IsEnabled )
			{
				Bs.BarcodeEntry.IsEnabled     = IsEnabled;
				( (ContentView)Bs ).IsEnabled = IsEnabled;
			}
		}

		public new static readonly BindableProperty IsEnabledProperty = BindableProperty.Create( nameof( IsVisible ),
		                                                                                         typeof( bool ),
		                                                                                         typeof( BarcodeScanner ),
		                                                                                         false,
		                                                                                         propertyChanged: IsEnabledPropertyChanged );

		public new bool IsEnabled
		{
			get => (bool)GetValue( IsEnabledProperty );
			set => SetValue( IsEnabledProperty, value );
		}
	#endregion

	#region Interface
	#region Scanning
		private bool _Scanning;

		public bool Scanning
		{
			get => _Scanning;
			set
			{
				_Scanning = value;
				Model.OnIsScanningChange( value );
			}
		}
	#endregion

		public string Barcode => Model.Barcode;

		public string Placeholder
		{
			get => BarcodeEntry.Placeholder;
			set => BarcodeEntry.Placeholder = value;
		}

		public Color PlaceholderTextColor
		{
			get => BarcodeEntry.PlaceholderColor;
			set => BarcodeEntry.PlaceholderColor = value;
		}

		public Action<string>? OnBarcode { get; set; }

		public ContentView ScannerView
		{
			get => this;
			set { }
		}

		public void Dispose()
		{
		}
	#endregion
	}
}