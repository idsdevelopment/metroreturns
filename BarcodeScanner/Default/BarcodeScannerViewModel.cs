﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using BarcodeInterface;
using Utils;
using XamlViewModel.Xamarin;

namespace DroidAz.Controls.Default
{
	public class BarcodeScannerViewModel : ViewModelBase
	{
		public IBarcodeScanner? OwnerInterface;

	#region Manual Barcode Input
		public bool ManualBarcodeInput
		{
			get { return Get( () => ManualBarcodeInput, false ); }
			set { Set( () => ManualBarcodeInput, value ); }
		}
	#endregion

	#region Fifo
		private void UpdateFifo( string barcode )
		{
			static void ClearFifo()
			{
				lock( BarcodeFifo )
					BarcodeFifo.Clear();
			}

			if( IsScanning )
			{
				lock( BarcodeFifo )
				{
					BarcodeFifo.AddLast( barcode );

					if( BarcodeFifo.Count == 1 ) // Need to start unload task
					{
						Tasks.RunVoid( () =>
						               {
							               try
							               {
								               while( IsScanning )
								               {
									               string BCode;

									               lock( BarcodeFifo )
									               {
										               if( BarcodeFifo.Count == 0 )
											               return;

										               BCode = BarcodeFifo.First.Value;
										               BarcodeFifo.RemoveFirst();
									               }

									               if( OnBarcode is not null )
									               {
										               var Code = BCode;
										               Dispatcher( () => { OnBarcode( Code ); } );
									               }
								               }
							               }
							               finally
							               {
								               ClearFifo();
							               }
						               } );
					}
				}
			}
			else
				ClearFifo();
		}
	#endregion

	#region Buttons
		public ICommand OnAccept => Commands[ nameof( OnAccept ) ];

		public void Execute_OnAccept()
		{
			UpdateFifo();
		}
	#endregion


	#region ZXing
		public Func<Task<string>>? ExecuteZXingScanner;

		public ICommand ZXingScanner => Commands[ nameof( ZXingScanner ) ];

		public async void Execute_ZXingScanner()
		{
			if( ExecuteZXingScanner is not null )
			{
				var BCode = await ExecuteZXingScanner.Invoke();
				UpdateFifo( BCode );
			}
		}
	#endregion

	#region Scanning
		private bool IsScanning;

		public bool DisableScanning
		{
			get { return Get( () => DisableScanning, false ); }
			set { Set( () => DisableScanning, value ); }
		}
	#endregion

	#region Events
		internal Action<string>? OnBarcode;

		public void OnIsScanningChange( bool scanning )
		{
			IsScanning      = scanning;
			DisableScanning = !scanning;
		}
	#endregion

	#region Barcode
		private static readonly LinkedList<string> BarcodeFifo = new();

		public string Barcode
		{
			get { return Get( () => Barcode, "" ); }
			set { Set( () => Barcode, value ); }
		}

		private void UpdateFifo()
		{
			var BCode = Barcode.Trim();

			if( BCode.IsNotNullOrWhiteSpace() )
				UpdateFifo( BCode );

			Barcode = "";
		}

		[DependsUpon350( nameof( Barcode ) )]
		public void WhenBarcodeChanges()
		{
			if( !ManualBarcodeInput )
				UpdateFifo();
		}
	#endregion
	}
}