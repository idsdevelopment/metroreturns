﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace Transitions
{
	public static class Extensions
	{
		public static async Task DoFade( this VisualElement element, bool fadeIn, double toOpacity = 1 )
		{
			await element.FadeTo( fadeIn ? toOpacity : 0, 500, fadeIn ? Easing.CubicIn : Easing.CubicOut );
		}

		public static async Task RaiseAndFade( this Layout layout, View view, bool fadeIn, double toOpacity = 1 )
		{
			if( fadeIn )
			{
				view.IsVisible = true;
				layout.RaiseChild( view );
			}

			await view.DoFade( fadeIn, toOpacity );

			if( !fadeIn )
				view.IsVisible = false;
		}

		public static async Task RaiseAndFade( this Layout layout, View[] fromViews, View toView, double toOpacity = 1 )
		{
			var L = fromViews.Length;

			if( L > 0 )
			{
				var Tasks = new Task[ fromViews.Length + 1 ];
				Tasks[ 0 ] = layout.RaiseAndFade( toView, true, toOpacity );

				for( var I = 0; I < L; )
				{
					var V = fromViews[ I++ ];
					Tasks[ I ] = layout.RaiseAndFade( V, false, toOpacity );
				}

				await Task.WhenAll( Tasks );
			}
		}

		public static async Task RaiseAndFade( this Layout layout, View fromView, View toView, double toOpacity = 1 )
		{
			await layout.RaiseAndFade( new[] {fromView}, toView, toOpacity );
		}
	}
}