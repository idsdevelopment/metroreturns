﻿namespace Globals;

public static class Orientation
{
	public static Action<ORIENTATION>? ChangeOrientation;

	public static ORIENTATION LayoutDirection
	{
		get;
		set
		{
			if( value != field )
			{
				field = value;
				ChangeOrientation?.Invoke( value );
			}
		}
	} = ORIENTATION.BOTH;

	public enum ORIENTATION
	{
		BOTH,
		PORTRAIT,
		LANDSCAPE
	}
}