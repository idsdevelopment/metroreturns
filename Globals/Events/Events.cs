﻿namespace Globals;

public class Events
{
	public static readonly WeakEvent<bool> OnInternetEvent = new(),
	                                       OnPingEvent     = new();
}