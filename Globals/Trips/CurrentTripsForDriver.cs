﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IdsRemoteService;
using Utils;

namespace Globals.Trips;

public class CurrentTripsForDriver
{
	public static readonly WeakEvent<IList<TripDisplayEntry>> NewTripsEvent     = new(),
															  UpdatedTripsEvent = new();

	public static readonly WeakEvent<IList<string>> DeletedTripsEvent = new();

	public static readonly Service.STATUS[] DefaultStatusFilter =
	{
		Service.STATUS.DISPATCHED,
		Service.STATUS.PICKED_UP
	};

	public static ObservableCollection<TripDisplayEntry> Trips
	{
		get
		{
			lock( CurrentTrips )
				return new ObservableCollection<TripDisplayEntry>( CurrentTrips );
		}
	}

	private static readonly List<TripDisplayEntry> CurrentTrips = new();

	private static readonly Timer PollingTimer = new( 60 * 60 * 1000 )
												 {
													 AutoReset = true
												 };

	private static volatile bool OnLine = true,
								 InPollingEvent;

	static CurrentTripsForDriver()
	{
		Events.OnPingEvent.Add( ( _, online ) =>
								{
									OnLine = online;
								} );

		PollingTimer.Elapsed += OnPollingTimerOnElapsed;
		PollingTimer.Start();
	}

	public static void ReplaceTrips( ICollection<TripDisplayEntry> trips )
	{
		lock( CurrentTrips )
		{
			var Replaced = false;

			for( int Ndx = 0, Count = CurrentTrips.Count; Ndx < Count; Ndx++ )
			{
				var Ct = CurrentTrips[ Ndx ].TripId;

				foreach( var Trip in trips )
				{
					if( Trip.TripId == Ct )
					{
						CurrentTrips[ Ndx ] = Trip;
						Replaced            = true;
						break;
					}
				}
			}

			if( Replaced )
				UpdatedTripsEvent.Value = trips.ToList();
		}
	}

	public static bool RemoveTrip( string tripId )
	{
		lock( CurrentTrips )
		{
			var Ct = CurrentTrips;

			int Ndx   = 0,
				Count = CurrentTrips.Count;

			var Found = false;

			for( ; Ndx < Count; Ndx++ )
			{
				if( Ct[ Ndx ].TripId == tripId )
				{
					Found = true;
					break;
				}
			}

			if( Found )
			{
				Ct.RemoveAt( Ndx );
				DeletedTripsEvent.Value = new List<string> {tripId};
			}
			return Found;
		}
	}

	public static bool PollTrips( Service.STATUS[]? filter = null )
	{
		OnPollingTimerOnElapsed();
		return HasTrips( filter );
	}

	public static bool HasTrips( Service.STATUS[]? filter = null )
	{
		filter ??= DefaultStatusFilter;

		lock( CurrentTrips )
		{
			if( CurrentTrips.Count > 0 )
			{
				foreach( var Trip in CurrentTrips )
				{
					if( filter.Contains( Trip.Status ) )
						return true;
				}
			}
		}
		return false;
	}

	public static IList<TripDisplayEntry> GetTrips( Service.STATUS[]? filter = null )
	{
		filter ??= DefaultStatusFilter;

		lock( CurrentTrips )
		{
			return ( from T in CurrentTrips
					 where filter.Contains( T.Status )
					 select T ).ToList();
		}
	}

	private static async void OnPollingTimerOnElapsed( object? o = null, ElapsedEventArgs? elapsedEventArgs = null )
	{
		if( !InPollingEvent )
		{
			InPollingEvent = true;

			try
			{
				if( OnLine && Login.SignedIn )
				{
					var NewTrips = Service.Client.FindTripsForDriver( Login.UserName );

					var TripList = new List<(bool ToDelete, bool Update, bool New, TripDisplayEntry Trip)>();

					lock( CurrentTrips )
						TripList.AddRange( CurrentTrips.Select( tripDisplayEntry => ( true, false, false, TripDisplayEntry: tripDisplayEntry ) ) );

					for( int Index = 0, Count = TripList.Count; Index < Count; Index++ )
					{
						var Update = false;

						var UpdateTrip = TripList[ Index ];

						var Trip        = UpdateTrip.Trip;
						var UTripId     = Trip.TripId;
						var LastUpdated = Trip.OriginalRemoteTrip?.lastUpdated ?? DateTime.MinValue.UnixTicksMilliSeconds();

						foreach( var NewTrip in NewTrips )
						{
							if( UTripId == NewTrip.tripId )
							{
								UpdateTrip.ToDelete = false;
								UpdateTrip.Update   = NewTrip.lastUpdated != LastUpdated;
								Update              = true;
							}
						}

						if( Update )
							TripList[ Index ] = UpdateTrip;
					}

					// Add New
					foreach( var NewTrip in NewTrips )
					{
						var Found = false;

						foreach( var Trip in TripList )
						{
							if( Trip.Trip.TripId == NewTrip.tripId )
							{
								Found = true;
								break;
							}
						}

						if( !Found )
							TripList.Add( ( false, false, true, await NewTrip.Convert() ) );
					}

					lock( CurrentTrips )
					{
						CurrentTrips.Clear();

						CurrentTrips.AddRange( from T in TripList
											   where !T.ToDelete
											   select T.Trip );
					}

					// Do Deletes First
					DeletedTripsEvent.Value = ( from T in TripList
												where T.ToDelete
												select T.Trip.TripId ).ToList();

					UpdatedTripsEvent.Value = ( from T in TripList
												where T.Update
												select T.Trip ).ToList();

					NewTripsEvent.Value = ( from T in TripList
											where T.New
											select T.Trip ).ToList();
				}
			}
			finally
			{
				InPollingEvent = false;
			}
		}
	}
}