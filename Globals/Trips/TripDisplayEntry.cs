﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using IdsRemoteService.org.internetdispatcher.jbtest2;
using Utils;
using static IdsRemoteService.Service;

// ReSharper disable InconsistentNaming

namespace Globals.Trips;

public class SignatureResult
{
	public int Height,
	           Width;

	public string Points = "";
}

public static partial class Extensions
{
	public static remoteTripDetailed ToRemoteTripDetailed( this remoteTrip t ) => t.ToRemoteTripDetailed( TripDisplayEntry.TOTE_RESELLER_ID,
	                                                                                                      TripDisplayEntry.TOTE_ACCOUNT_ID,
	                                                                                                      "",
	                                                                                                      TripDisplayEntry.TOTE_ADDRESS,
	                                                                                                      TripDisplayEntry.TOTE_CITY,
	                                                                                                      TripDisplayEntry.TOTE_REGION,
	                                                                                                      "",
	                                                                                                      TripDisplayEntry.TOTE_POST_CODE
	                                                                                                    );

	public static remoteTripDetailed ToRemoteTripDetailed( this remoteTrip t,
	                                                       string deliveryResellerId,
	                                                       string deliveryAccountId,
	                                                       string deliverySuite,
	                                                       string deliveryStreet,
	                                                       string deliveryCity,
	                                                       string deliveryProvince,
	                                                       string deliveryCountry,
	                                                       string deliveryPc
	) => t.ToRemoteTripDetailed( deliveryResellerId, deliveryAccountId, deliverySuite, deliveryStreet, deliveryCity, deliveryProvince, deliveryCountry, deliveryPc,
	                             deliveryResellerId, deliveryAccountId, deliverySuite, deliveryStreet, deliveryCity, deliveryProvince, deliveryCountry, deliveryPc );

	public static remoteTripDetailed ToRemoteTripDetailed( this remoteTrip t,
	                                                       string deliveryResellerId,
	                                                       string deliveryAccountId,
	                                                       string deliverySuite,
	                                                       string deliveryStreet,
	                                                       string deliveryCity,
	                                                       string deliveryProvince,
	                                                       string deliveryCountry,
	                                                       string deliveryPc,
	                                                       string pickupResellerId,
	                                                       string pickupAccountId,
	                                                       string pickupSuite,
	                                                       string pickupStreet,
	                                                       string pickupCity,
	                                                       string pickupProvince,
	                                                       string pickupCountry,
	                                                       string pickupPc ) => new()
	                                                                            {
		                                                                            deliveryResellerId  = deliveryResellerId,
		                                                                            deliveryAccountId   = deliveryAccountId,
		                                                                            deliveryAddressId   = t.deliveryAddressId,
		                                                                            deliverySuite       = deliverySuite,
		                                                                            deliveryStreet      = deliveryStreet,
		                                                                            deliveryCity        = deliveryCity,
		                                                                            deliveryProvince    = deliveryProvince,
		                                                                            deliveryCountry     = deliveryCountry,
		                                                                            deliveryPc          = deliveryPc,
		                                                                            deliveryDescription = "",

		                                                                            pickupResellerId  = pickupResellerId,
		                                                                            pickupAccountId   = pickupAccountId,
		                                                                            pickupAddressId   = t.pickupAddressId,
		                                                                            pickupSuite       = pickupSuite,
		                                                                            pickupStreet      = pickupStreet,
		                                                                            pickupCity        = pickupCity,
		                                                                            pickupProvince    = pickupProvince,
		                                                                            pickupCountry     = pickupCountry,
		                                                                            pickupPc          = pickupPc,
		                                                                            pickupDescription = "",

		                                                                            driver = t.driver,
		                                                                            tripId = t.tripId,
		                                                                            status = t.status,

		                                                                            lastUpdated                       = t.lastUpdated,
		                                                                            serviceLevel                      = t.serviceLevel,
		                                                                            resellerId                        = t.resellerId,
		                                                                            deliveryCompany                   = t.deliveryCompany,
		                                                                            pickupCompany                     = t.pickupCompany,
		                                                                            POD                               = t.POD,
		                                                                            accountId                         = t.accountId,
		                                                                            billingNotes                      = t.billingNotes,
		                                                                            board                             = t.board,
		                                                                            callTakerId                       = t.callTakerId,
		                                                                            callTime                          = t.callTime,
		                                                                            callTimeSpecified                 = t.callTimeSpecified,
		                                                                            caller                            = t.caller,
		                                                                            callerEmail                       = t.callerEmail,
		                                                                            callerPhone                       = t.callerPhone,
		                                                                            carCharge                         = t.carCharge,
		                                                                            carChargeAmount                   = t.carChargeAmount,
		                                                                            clientReference                   = t.clientReference,
		                                                                            currentZone                       = t.currentZone,
		                                                                            dangerousGoods                    = t.dangerousGoods,
		                                                                            deadlineTime                      = t.deadlineTime,
		                                                                            deadlineTimeSpecified             = t.deadlineTimeSpecified,
		                                                                            declaredValueAmount               = t.declaredValueAmount,
		                                                                            deliveredTime                     = t.deliveredTime,
		                                                                            deliveredTimeSpecified            = t.deliveredTimeSpecified,
		                                                                            deliveryAmount                    = t.deliveryAmount,
		                                                                            deliveryArriveTime                = t.deliveryArriveTime,
		                                                                            deliveryArriveTimeSpecified       = t.deliveryArriveTimeSpecified,
		                                                                            deliveryContact                   = t.deliveryContact,
		                                                                            deliveryEmail                     = t.deliveryEmail,
		                                                                            deliveryNotes                     = t.deliveryNotes,
		                                                                            deliveryPhone                     = t.deliveryPhone,
		                                                                            deliveryZone                      = t.deliveryZone,
		                                                                            disbursementAmount                = t.disbursementAmount,
		                                                                            discountAmount                    = t.discountAmount,
		                                                                            disputed                          = t.disputed,
		                                                                            dontFinalizeDate                  = t.dontFinalizeDate,
		                                                                            dontFinalizeDateSpecified         = t.dontFinalizeDateSpecified,
		                                                                            dontFinalizeFlag                  = t.dontFinalizeFlag,
		                                                                            dontFinalizeFlagSpecified         = t.dontFinalizeFlagSpecified,
		                                                                            driverAssignTime                  = t.driverAssignTime,
		                                                                            driverAssignTimeSpecified         = t.driverAssignTimeSpecified,
		                                                                            driverPaidCommissionDate          = t.driverPaidCommissionDate,
		                                                                            driverPaidCommissionDateSpecified = t.driverPaidCommissionDateSpecified,
		                                                                            height                            = t.height,
		                                                                            insuranceAmount                   = t.insuranceAmount,
		                                                                            internalId                        = t.internalId,
		                                                                            invoiceId                         = t.invoiceId,
		                                                                            invoiced                          = t.invoiced,
		                                                                            isCashTrip                        = t.isCashTrip,
		                                                                            isCashTripReconciled              = t.isCashTripReconciled,
		                                                                            isDriverPaid                      = t.isDriverPaid,
		                                                                            length                            = t.length,
		                                                                            mailDrop                          = t.mailDrop,
		                                                                            miscAmount                        = t.miscAmount,
		                                                                            modified                          = t.modified,
		                                                                            modifiedTripDate                  = t.modifiedTripDate,
		                                                                            modifiedTripDateSpecified         = t.modifiedTripDateSpecified,
		                                                                            modifiedTripFlag                  = t.modifiedTripFlag,
		                                                                            modifiedTripFlagSpecified         = t.modifiedTripFlagSpecified,
		                                                                            noGoodsAmount                     = t.noGoodsAmount,
		                                                                            packageType                       = t.packageType,
		                                                                            pallets                           = t.pallets,
		                                                                            pickupArriveTime                  = t.pickupArriveTime,
		                                                                            pickupArriveTimeSpecified         = t.pickupArriveTimeSpecified,
		                                                                            pickupContact                     = t.pickupContact,
		                                                                            pickupEmail                       = t.pickupEmail,
		                                                                            pickupNotes                       = t.pickupNotes,
		                                                                            pickupPhone                       = t.pickupPhone,
		                                                                            pickupTime                        = t.pickupTime,
		                                                                            pickupTimeSpecified               = t.pickupTimeSpecified,
		                                                                            pickupZone                        = t.pickupZone,
		                                                                            pieces                            = t.pieces,
		                                                                            podName                           = t.podName,
		                                                                            priorityInvoiceId                 = t.priorityInvoiceId,
		                                                                            priorityInvoiceIdSpecified        = t.priorityInvoiceIdSpecified,
		                                                                            priorityStatus                    = t.priorityStatus,
		                                                                            priorityStatusSpecified           = t.priorityStatusSpecified,
		                                                                            processedByIdsRouteDate           = t.processedByIdsRouteDate,
		                                                                            processedByIdsRouteDateSpecified  = t.processedByIdsRouteDateSpecified,
		                                                                            readyTime                         = t.readyTime,
		                                                                            readyTimeSpecified                = t.readyTimeSpecified,
		                                                                            readyTimeString                   = t.readyTimeString,
		                                                                            readyToInvoiceFlag                = t.readyToInvoiceFlag,
		                                                                            readyToInvoiceFlagSpecified       = t.readyToInvoiceFlagSpecified,
		                                                                            receivedByIdsRouteDate            = t.receivedByIdsRouteDate,
		                                                                            receivedByIdsRouteDateSpecified   = t.receivedByIdsRouteDateSpecified,
		                                                                            redirect                          = t.redirect,
		                                                                            redirectAmount                    = t.redirectAmount,
		                                                                            returnTrip                        = t.returnTrip,
		                                                                            sigFilename                       = t.sigFilename,
		                                                                            skipUpdateCommonAddress           = t.skipUpdateCommonAddress,
		                                                                            sortOrder                         = t.sortOrder,
		                                                                            stopGroupNumber                   = t.stopGroupNumber,
		                                                                            totalAmount                       = t.totalAmount,
		                                                                            totalFixedAmount                  = t.totalFixedAmount,
		                                                                            totalPayrollAmount                = t.totalPayrollAmount,
		                                                                            totalTaxAmount                    = t.totalTaxAmount,
		                                                                            waitTimeAmount                    = t.waitTimeAmount,
		                                                                            waitTimeMins                      = t.waitTimeMins,
		                                                                            weight                            = t.weight,
		                                                                            weightAmount                      = t.weightAmount,
		                                                                            weightOrig                        = t.weightOrig,
		                                                                            width                             = t.width
	                                                                            };
}

public static partial class Extensions
{
	public static readonly string[] NOT_ALLOWED =
	{
		TripDisplayEntry.CWD_SHIPMENT,
		TripDisplayEntry.RETURN,
		TripDisplayEntry.ASSET
	};

	public static bool IsCTT( this TripDisplayEntry t ) => t.Status is STATUS.PICKED_UP && t.TripId.StartsWith( "CTT", StringComparison.OrdinalIgnoreCase ) && IsRequired();
	public static bool IsRequired() => Login.Required; // Asset pickup Driver

	public static bool IsAsset( this string serviceLevel ) => string.Compare( serviceLevel, TripDisplayEntry.ASSET, StringComparison.OrdinalIgnoreCase ) == 0;
	public static bool IsAsset( this TripDisplayEntry t ) => t.Status is STATUS.DISPATCHED or STATUS.PICKED_UP && t.ServiceLevel.IsAsset();
	public static bool IsDispatchedAsset( this TripDisplayEntry t ) => t.Status is STATUS.DISPATCHED && t.ServiceLevel.IsAsset();
	public static bool IsPickedUpAsset( this TripDisplayEntry t ) => t.Status is STATUS.PICKED_UP && t.ServiceLevel.IsAsset();

	public static bool IsReturn( this string serviceLevel ) => string.Compare( serviceLevel, TripDisplayEntry.RETURN, StringComparison.OrdinalIgnoreCase ) == 0;
	public static bool IsReturn( this TripDisplayEntry t ) => t.Status is STATUS.DISPATCHED or STATUS.PICKED_UP && t.ServiceLevel.IsReturn();
	public static bool IsPickedUpReturn( this TripDisplayEntry t ) => t.Status is STATUS.PICKED_UP && t.ServiceLevel.IsReturn();

	public static bool IsDsd( this string serviceLevel ) => !NOT_ALLOWED.Contains( serviceLevel.Trim(), StringComparison.OrdinalIgnoreCase );

	public static bool IsDsd( this TripDisplayEntry t ) => t.Status is STATUS.DISPATCHED or STATUS.PICKED_UP && t.ServiceLevel.IsDsd();

	public static remoteTrip ConvertToRemoteTrip( this TripDisplayEntry trip, SignatureResult signature, string driver, STATUS status )
	{
		var T   = trip.ConvertToRemoteTrip();
		var Now = DateTime.Now;

		T.driver          = driver;
		T.clientReference = trip.Barcode;
		T.pallets         = trip.Pallets;

		T.status = (int)status;

		switch( status )
		{
		case STATUS.DELIVERED:
		case STATUS.VERIFIED:
			T.deliveredTime          = Now;
			T.deliveredTimeSpecified = true;
			break;

		case STATUS.CREATE_NEW_TRIP:
			T.callTime          = Now;
			T.callTimeSpecified = true;
			break;

		default:
			T.pickupTime       = Now;
			T.pickupArriveTime = Now;

			T.pickupTimeSpecified       = true;
			T.pickupArriveTimeSpecified = true;
			break;
		}

		T.POD     = false;
		T.podName = trip.PopPod;

		T.sigFilename = signature.Points;
		T.sigHeight   = signature.Height;
		T.sigWidth    = signature.Width;

		T.modified                  = true;
		T.modifiedTripDate          = Now;
		T.modifiedTripDateSpecified = true;
		T.modifiedTripFlag          = true;
		T.modifiedTripFlagSpecified = true;

		return T;
	}

	public static remoteTrip ConvertToRemoteTrip( this TripDisplayEntry trip )
	{
		var T = trip.OriginalRemoteTrip!;

		return new remoteTrip
		       {
			       POD                               = T.POD,
			       accountId                         = T.accountId,
			       billingCompany                    = T.billingCompany,
			       billingNotes                      = T.billingNotes,
			       board                             = T.board,
			       callTakerId                       = T.callTakerId,
			       callTime                          = T.callTime,
			       callTimeSpecified                 = T.callTimeSpecified,
			       caller                            = T.caller,
			       callerEmail                       = T.callerEmail,
			       callerPhone                       = T.callerPhone,
			       carCharge                         = T.carCharge,
			       carChargeAmount                   = T.carChargeAmount,
			       clientReference                   = T.clientReference,
			       currentZone                       = T.currentZone,
			       dangerousGoods                    = T.dangerousGoods,
			       deadlineTime                      = T.deadlineTime,
			       deadlineTimeSpecified             = T.deadlineTimeSpecified,
			       declaredValueAmount               = T.declaredValueAmount,
			       deliveredTime                     = T.deliveredTime,
			       deliveredTimeSpecified            = T.deliveredTimeSpecified,
			       deliveryAddressId                 = T.deliveryAddressId,
			       deliveryAmount                    = T.deliveryAmount,
			       deliveryArriveTime                = T.deliveryArriveTime,
			       deliveryArriveTimeSpecified       = T.deliveryArriveTimeSpecified,
			       deliveryCompany                   = T.deliveryCompany,
			       deliveryContact                   = T.deliveryContact,
			       deliveryEmail                     = T.deliveryEmail,
			       deliveryNotes                     = T.deliveryNotes,
			       deliveryPhone                     = T.deliveryPhone,
			       deliveryZone                      = T.deliveryZone,
			       disbursementAmount                = T.disbursementAmount,
			       discountAmount                    = T.discountAmount,
			       disputed                          = T.disputed,
			       dontFinalizeDate                  = T.dontFinalizeDate,
			       dontFinalizeDateSpecified         = T.dontFinalizeDateSpecified,
			       dontFinalizeFlag                  = T.dontFinalizeFlag,
			       dontFinalizeFlagSpecified         = T.dontFinalizeFlagSpecified,
			       driver                            = T.driver,
			       driverAssignTime                  = T.driverAssignTime,
			       driverAssignTimeSpecified         = T.driverAssignTimeSpecified,
			       driverPaidCommissionDate          = T.driverPaidCommissionDate,
			       driverPaidCommissionDateSpecified = T.driverPaidCommissionDateSpecified,
			       height                            = T.height,
			       insuranceAmount                   = T.insuranceAmount,
			       internalId                        = T.internalId,
			       invoiceId                         = T.invoiceId,
			       invoiced                          = T.invoiced,
			       isCashTrip                        = T.isCashTrip,
			       isCashTripReconciled              = T.isCashTripReconciled,
			       isDriverPaid                      = T.isDriverPaid,
			       lastUpdated                       = T.lastUpdated,
			       length                            = T.length,
			       mailDrop                          = T.mailDrop,
			       miscAmount                        = T.miscAmount,

			       noGoodsAmount                    = T.noGoodsAmount,
			       packageType                      = T.packageType,
			       pallets                          = T.pallets,
			       pickupAddressId                  = T.pickupAddressId,
			       pickupArriveTime                 = T.pickupArriveTime,
			       pickupArriveTimeSpecified        = T.pickupArriveTimeSpecified,
			       pickupCompany                    = T.pickupCompany,
			       pickupContact                    = T.pickupContact,
			       pickupEmail                      = T.pickupEmail,
			       pickupNotes                      = T.pickupNotes,
			       pickupPhone                      = T.pickupPhone,
			       pickupTime                       = T.pickupTime,
			       pickupTimeSpecified              = T.pickupTimeSpecified,
			       pickupZone                       = T.pickupZone,
			       pieces                           = T.pieces,
			       podName                          = T.podName,
			       priorityInvoiceId                = T.priorityInvoiceId,
			       priorityInvoiceIdSpecified       = T.priorityInvoiceIdSpecified,
			       priorityStatus                   = T.priorityStatus,
			       priorityStatusSpecified          = T.priorityStatusSpecified,
			       processedByIdsRouteDate          = T.processedByIdsRouteDate,
			       processedByIdsRouteDateSpecified = T.processedByIdsRouteDateSpecified,
			       readyTime                        = T.readyTime,
			       readyTimeSpecified               = T.readyTimeSpecified,
			       readyTimeString                  = T.readyTimeString,
			       readyToInvoiceFlag               = T.readyToInvoiceFlag,
			       readyToInvoiceFlagSpecified      = T.readyToInvoiceFlagSpecified,
			       receivedByIdsRouteDate           = T.receivedByIdsRouteDate,
			       receivedByIdsRouteDateSpecified  = T.receivedByIdsRouteDateSpecified,
			       redirect                         = T.redirect,
			       redirectAmount                   = T.redirectAmount,
			       resellerId                       = T.resellerId,
			       returnTrip                       = T.returnTrip,
			       serviceLevel                     = T.serviceLevel,
			       sigFilename                      = T.sigFilename,
			       sigHeight                        = T.sigHeight,
			       sigWidth                         = T.sigWidth,
			       skipUpdateCommonAddress          = T.skipUpdateCommonAddress,
			       sortOrder                        = T.sortOrder,
			       status                           = T.status,
			       stopGroupNumber                  = T.stopGroupNumber,
			       totalAmount                      = T.totalAmount,
			       totalFixedAmount                 = T.totalFixedAmount,
			       totalPayrollAmount               = T.totalPayrollAmount,
			       totalTaxAmount                   = T.totalTaxAmount,
			       tripId                           = T.tripId,
			       waitTimeAmount                   = T.waitTimeAmount,
			       waitTimeMins                     = T.waitTimeMins,
			       weight                           = T.weight,
			       weightAmount                     = T.weightAmount,
			       weightOrig                       = T.weightOrig,
			       width                            = T.width,

			       modified                  = T.modified,
			       modifiedTripDate          = T.modifiedTripDate,
			       modifiedTripDateSpecified = T.modifiedTripDateSpecified,
			       modifiedTripFlag          = T.modifiedTripFlag,
			       modifiedTripFlagSpecified = T.modifiedTripFlagSpecified
		       };
	}

	public static async Task<TripDisplayEntry> Convert( this remoteTrip trip )
	{
		return await Task.Run( () =>
		                       {
			                       var DTrip = new TripDisplayEntry( trip );

			                       var Tasks = new Task<(bool, remoteAddress)>[]
			                                   {
				                                   Client.GetRemoteAddressAsync( DTrip.PickupAddress ),
				                                   Client.GetRemoteAddressAsync( DTrip.DeliveryAddress )
			                                   };

			                       Task.WaitAll( Tasks );

			                       string MakeAddress( Task<(bool ok, remoteAddress address)> address )
			                       {
				                       var (Ok, Address) = address.Result;

				                       if( Ok )
				                       {
					                       var Suite = Address.suite.NullTrim();

					                       if( Suite != "" )
						                       Suite += '/';
					                       return $"{Suite}{Address.street.NullTrim()} {Address.city.NullTrim()}".Trim();
				                       }
				                       return "";
			                       }

			                       DTrip.PickupAddress   = MakeAddress( Tasks[ 0 ] );
			                       DTrip.DeliveryAddress = MakeAddress( Tasks[ 1 ] );

			                       return DTrip;
		                       } );
	}

	public static async Task<List<TripDisplayEntry>> Convert( this List<remoteTrip> trips )
	{
		try
		{
			return await Task.Run( () =>
			                       {
				                       var Result = new List<TripDisplayEntry>();

				                       try
				                       {
					                       new Tasks.Task<remoteTrip>().Run( trips, trip =>
					                                                                {
						                                                                var Trip = trip.Convert().Result;

						                                                                lock( Result )
							                                                                Result.Add( Trip );
					                                                                }, 3 );
				                       }
				                       catch( Exception Exception )
				                       {
					                       Debug.WriteLine( Exception );
				                       }
				                       return Result;
			                       } );
		}
		catch( Exception Exception )
		{
			Debug.WriteLine( Exception );
		}
		return [];
	}
}

public record TripDisplayEntry : INotifyPropertyChanged
{
	public const string RETURN           = "RETURN",
	                    CWD_SHIPMENT     = "CWDSHIPMENT",
	                    TOTE_ASSET       = "asset",
	                    ASSET            = "ASSET",
	                    TOTE_COMPANY     = "(HubCA44) Rothmans Benson&Hedges Inc",
	                    TOTE_ADDRESS     = "1500 Don Mills Road",
	                    TOTE_CITY        = "1500 Don Mills Road",
	                    TOTE_REGION      = "Ontario",
	                    TOTE_POST_CODE   = "M5X 1A4",
	                    TOTE_ADDRESS_ID  = "15b8d6ea75061c4f01750f49cae04155",
	                    TOTE_RESELLER_ID = "metro",
	                    TOTE_ACCOUNT_ID  = "rbandh_metro",
	                    TOTE_ACCOUNT     = "Metro SCG";

	public string TripId { get; set; }

	public STATUS Status
	{
		get => _Status;
		set
		{
			_Status = value;
			OnPropertyChanged();
			OnPropertyChanged( nameof( IsPickup ) );
			OnPropertyChanged( nameof( IsDelivery ) );
		}
	}

	public string AccountId { get; set; }

	public string Reference
	{
		get => _Reference;
		set
		{
			_Reference = value;

			if( OriginalRemoteTrip is not null )
				OriginalRemoteTrip.clientReference = value;
			OnPropertyChanged();
		}
	}

	public bool IsCTT    => this.IsCTT();
	public bool IsAsset  => this.IsAsset();
	public bool IsReturn => this.IsReturn();
	public bool IsDsd    => this.IsDsd();

	public string PickupCompany        { get; set; }
	public string PickupAddress        { get; set; }
	public string DisplayPickupAddress => _DisplayPickupAddress ??= PickupAddress.Trim();
	public string PickupNotes          { get; set; }
	public string PickupLocation       { get; set; }

	public string DeliveryCompany        { get; set; }
	public string DeliveryAddress        { get; set; }
	public string DisplayDeliveryAddress => _DisplayDeliveryAddress ??= DeliveryAddress.Trim();
	public string DeliveryNotes          { get; set; }
	public string DeliveryLocation       { get; set; }

	public bool IsPickup   => Status < STATUS.PICKED_UP;
	public bool IsDelivery => !IsPickup;

	public string ServiceLevel { get; set; }
	public string PackageType  { get; set; }

	public decimal Weight { get; set; }
	public decimal Pieces { get; set; }

	public string PopPod { get; set; }

	public string DisplayCompany => IsPickup ? PickupCompany : DeliveryCompany;

	public string DisplayAddress => IsPickup ? DisplayPickupAddress : DisplayDeliveryAddress;

	public string Driver { get; set; }

	public string Barcode
	{
		get => _Barcode;
		set
		{
			_Barcode = value;
			OnPropertyChanged();
		}
	}

	public bool IsScanned
	{
		get => _IsScanned;
		set
		{
			_IsScanned = value;
			OnPropertyChanged();
		}
	}

	public          string      Pallets { get; set; }
	public readonly remoteTrip? OriginalRemoteTrip;

	private string _Barcode;

	private string? _DisplayPickupAddress,
	                _DisplayDeliveryAddress;

	private bool _IsScanned;

	private string _Reference;

	private STATUS _Status;

	public TripDisplayEntry( TripDisplayEntry t )
	{
		_Barcode   = "Not Scanned";
		_Reference = "";

		OriginalRemoteTrip = t.OriginalRemoteTrip;

		TripId = t.TripId;
		Status = t.Status;

		AccountId = t.AccountId;
		Reference = t.Reference;

		PickupCompany  = t.PickupCompany;
		PickupAddress  = t.PickupAddress;
		PickupNotes    = t.PickupNotes;
		PickupLocation = t.PickupLocation;

		DeliveryCompany  = t.DeliveryCompany;
		DeliveryAddress  = t.DeliveryAddress;
		DeliveryNotes    = t.DeliveryNotes;
		DeliveryLocation = t.DeliveryLocation;

		ServiceLevel = t.ServiceLevel;

		PackageType = t.PackageType;

		PopPod = t.PopPod;

		Weight = t.Weight;
		Pieces = t.Pieces;

		Pallets = t.Pallets;

		Driver = t.Driver;
	}

	public TripDisplayEntry()
	{
		_Barcode   = "Not Scanned";
		_Reference = "";

		TripId             = "";
		AccountId          = "";
		PickupCompany      = "";
		PickupAddress      = "";
		PickupNotes        = "";
		PickupLocation     = "";
		DeliveryCompany    = "";
		DeliveryAddress    = "";
		DeliveryNotes      = "";
		DeliveryLocation   = "";
		ServiceLevel       = "";
		PackageType        = "";
		PopPod             = "";
		Pallets            = "";
		Driver             = "";
		OriginalRemoteTrip = null;
	}

	public TripDisplayEntry( remoteTrip t )
	{
		_Barcode   = "Not Scanned";
		_Reference = "";

		OriginalRemoteTrip = t;

		TripId = t.tripId.Trim();
		Status = (STATUS)t.status;

		AccountId = t.accountId.Trim();
		Reference = t.clientReference.NullTrim();

		PickupCompany  = t.pickupCompany.Trim();
		PickupAddress  = t.pickupAddressId;
		PickupNotes    = t.pickupNotes.NullTrim();
		PickupLocation = GetLocation( t.pickupCompany );

		DeliveryCompany  = t.deliveryCompany.Trim();
		DeliveryAddress  = t.deliveryAddressId;
		DeliveryNotes    = t.deliveryNotes.NullTrim();
		DeliveryLocation = GetLocation( t.deliveryCompany );

		ServiceLevel = t.serviceLevel.Trim();
		PackageType  = t.packageType.Trim();

		PopPod = "";

		Weight = (decimal)t.weight;
		Pieces = t.pieces;

		Pallets = t.pallets.NullTrim();

		Driver = t.driver.NullTrim();
	}

	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	private static string GetLocation( string companyName )
	{
		var P1 = companyName.IndexOf( '(' );

		if( P1 >= 0 )
		{
			var P2 = companyName.IndexOf( ')' );

			if( ( P2 >= 0 ) && ( P2 > P1 ) )
			{
				var Len = P2 - P1 - 1;

				if( Len > 0 )
					return companyName.Substring( P1 + 1, Len ).Trim();
			}
		}
		return "";
	}

	public event PropertyChangedEventHandler? PropertyChanged;

#region Modified / Removed
	private bool _Modified,
	             _Removed;

	public bool Modified
	{
		get => _Modified;
		set
		{
			_Modified = value;
			_Removed  = false;
			OnPropertyChanged();
		}
	}

	public bool Removed
	{
		get => _Removed;
		set
		{
			_Removed  = value;
			_Modified = false;
			OnPropertyChanged();
		}
	}
#endregion

#region Trip Id
	private string? _DisplayTripId;

	private string MakeTripId() => ( ShowReferenceInsteadOfTripId
		                                 ? IsReturn && Reference.IsNullOrWhiteSpace()
			                                   ? "-- Double Tap And Scan --" : Reference
		                                 : TripId ).Trim();

	private bool _ShowReferenceInsteadOfTripId;

	public bool ShowReferenceInsteadOfTripId
	{
		get => _ShowReferenceInsteadOfTripId;
		set
		{
			_ShowReferenceInsteadOfTripId = value;
			_DisplayTripId                = null;
			OnPropertyChanged( nameof( DisplayTripId ) );
		}
	}

	public string DisplayTripId => _DisplayTripId ??= MakeTripId();
#endregion

#region IsVisible
	private bool _IsVisible = true;

	public virtual bool IsVisible
	{
		get => _IsVisible;
		set
		{
			_IsVisible = value;
			OnPropertyChanged();
		}
	}
#endregion

#region Hide Show
	private bool _Hidden;

	public bool Hidden
	{
		get => _Hidden;
		set
		{
			if( value != _Hidden )
			{
				_Hidden = value;
				OnPropertyChanged();
				OnPropertyChanged( nameof( Showing ) );
			}
		}
	}

	public bool Showing
	{
		get => !_Hidden;
		set => Hidden = !value;
	}
#endregion
}