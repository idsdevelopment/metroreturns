﻿using System.Diagnostics;

namespace Globals;

public class Application
{
	public static void ExitApplication()
	{
		Process.GetCurrentProcess().Kill();
	}
}