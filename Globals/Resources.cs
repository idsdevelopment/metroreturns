﻿namespace Globals;

public static class Resources
{
	public static object GetResource( string resourceName ) => !Xamarin.Forms.Application.Current.Resources.TryGetValue( resourceName, out var ResValue ) ? resourceName : ResValue;
	public static string GetStringResource( string resourceName ) => (string)GetResource( resourceName );
}